import React from "react";
import axios from "axios";
// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  CardFooter,
  Col,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
} from "reactstrap";
// layout for this page
import Router from "next/router";
import {loadAuth} from '../../../utils/user.js';
import {truncStringPortion} from '../../../utils/helper.js';
import * as tezos from 'conseiljs';
import {storeAuth} from '../../../utils/user.js';

const derivationPath = "44'/1729'/0'/0'/0'";

class Call extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pending: false,
      error: null,
      ledgerKeystore: null,
      password: null,
      encryptedWallet: null,
      entries: {},
      parameters: {},
      feesTez: 0,
      burnTez: 0,
      storageCost: 0,
      ready: false
    };
    this.hiddenFileInput = React.createRef();
  };

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  updateEntry = (e, name) => {
    let entries = this.state.entries;
    entries[name] = e.target.value;
    this.setState({entries: entries});
  }

  unlockLedger = async () => {
        this.setState({loading: true, error: null});
        try{
            const keystore = await tezos.TezosLedgerWallet.unlockAddress(0, derivationPath);
            this.setState({loading: false, ledgerKeystore: keystore, error: null});
        }catch(error){
            console.log(error);
            this.setState({loading: false, error: 'Could not connect Ledger device'});
        }
  }

  handleUpload = (event) => {
    this.hiddenFileInput.current.click();
  };

  onFileHandler = (event) => {
    const reader = new FileReader();
    reader.addEventListener('load', event => {
        try{
            const data = event.target.result.split(';base64,').pop();
            const buff = new Buffer(data, 'base64');
            const jsonData = JSON.parse(buff.toString('ascii'));
            this.setState({
                encryptedWallet: jsonData,
                errorFile: false
            });
            const keystoreAuth = {
                type: ["file"],
                value: [this.props.userData.value[this.props.indexer]],
                keystore: [jsonData]
            }
            storeAuth(keystoreAuth);
        }catch(error){
            console.log(error);
            this.setState({errorFile: true});
        }
    });
    this.setState({fileName: event.target.files[0].name});
    reader.readAsDataURL(event.target.files[0]);
  }

  buildParameters = () => {
        let params = [];
        this.props.feature.values.forEach(entry => {
            if(!this.state.entries[entry.name]){
                this.setState({error: `No value for ${entry.name.toString()}`});
                return;
            }else if(entry.name !== 'ꜩAmount'){
                if(entry.type === 'bool')
                    params.push(this.state.entries[entry.name]);
                else if(Number.isNaN(parseFloat(this.state.entries[entry.name])))
                    params.push('"'+this.state.entries[entry.name]+'"');
                else
                    params.push(parseFloat(this.state.entries[entry.name]));
            }
        });
        return this.entryParsing(this.props.selectedToken.parameters, this.props.feature.entrypoint, params);
    }

  entryParsing = (parameters, entrypoint, params) => {
        const entryPoints = tezos.TezosContractIntrospector.generateEntryPointsFromParams(parameters);
        var i = 0;
        while(i < entryPoints.length){
            if(entryPoints[i].name === entrypoint)
                break
            i++;
        }
        return entryPoints[i].generateInvocationPair(...params);
    }

  ledgerSendOp= () => {
        if(!this.state.ledgerKeystore)
            this.setState({error: 'Please connect your ledger first'});
        else{
            const params = this.buildParameters();
            console.log(params);
            this.setState({pending: true, error: null, ready: false});
            let amountTez = 0;
            this._asyncRequest = tezos.TezosNodeWriter.testContractInvocationOperation(this.props.tezosNodes[0], 'main', this.state.ledgerKeystore, this.props.selectedToken.contract_hash, 0, 300000, 2000, 500000, params.entrypoint, params.parameters, tezos.TezosParameterFormat.Michelson).then(
                result => {
                    console.log(JSON.stringify(result));
                    const burnTez = result.gas + 100;
                    const feesTez = Math.round(result.gas/10 + 8000);
                    this.setState({burnTez: burnTez, feesTez: feesTez, storageCost: result.storageCost, parameters: params, pending: false, ready: true});
                }
            ).catch(error => {
                console.log(error);
                this.setState({errorCall: 'Simulation could not be processed', pending: false});
            });
        }
    }

  keystoreSimulateOp= () => {
        if(!this.props.userData.type[this.props.indexer] === 'file')
            this.setState({error: 'Please select a Keystore wallet file'});
        else if(!this.state.password || (JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}' && ! this.state.encryptedWallet))
            this.setState({error: 'Missing parameters'});
        else{
            const params = this.buildParameters();
            console.log(params);
            this.setState({pending: true, error: null, ready: false});
            const keystore = JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}'? this.state.encryptedWallet : this.props.userData.keystore[this.props.indexer];
            this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(keystore), this.state.password).then(loadedWallet => {
                let amountTez = 0;
                this._asyncRequest = tezos.TezosNodeWriter.testContractInvocationOperation(this.props.tezosNodes[0], 'main', loadedWallet.identities[0], this.props.selectedToken.contract_hash, 0, 300000, 2000, 500000, params.entrypoint, params.parameters, tezos.TezosParameterFormat.Michelson).then(
                    result => {
                        console.log(JSON.stringify(result));
                        const burnTez = result.gas + 100;
                        const feesTez = Math.round(result.gas/10 + 8000);
                        this.setState({burnTez: burnTez, feesTez: feesTez, storageCost: result.storageCost, parameters: params, pending: false, ready: true});
                    }
                ).catch(error => {
                    console.log(error);
                    this.setState({errorCall: 'Simulation could not be processed', pending: false});
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({error: 'Invalid password', pending: false});
            });
        }
    }

  sendKeystoreOperation = () => {
    const keystore = JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}'? this.state.encryptedWallet : this.props.userData.keystore[this.props.indexer];
    this.setState({pending: true});
    this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(keystore), this.state.password).then(loadedWallet => {
            console.log(`Sending ${this.state.parameters.entrypoint} operation to ${this.props.selectedToken.contract_hash} with fees to ${this.state.feesTez} and burn cap to ${this.state.burnTez}`);
            this._asyncRequest = tezos.TezosNodeWriter.sendContractInvocationOperation(this.props.tezosNodes[0], loadedWallet.identities[0], this.props.selectedToken.contract_hash, 0, this.state.feesTez, '', this.state.storageCost, this.state.burnTez, this.state.parameters.entrypoint, this.state.parameters.parameters, tezos.TezosParameterFormat.Michelson).then(
                operation => {
                    this.setState({pending: false});
                    this.props.renderSuccessMsg(`Call for <strong>${this.props.feature.entrypoint} ${this.props.selectedToken.token_symbol}</strong> sent with ID <small>${operation.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.state.entries['amount'] || this.state.entries['value']} ${this.props.selectedToken.token_symbol}</strong> to <strong>${this.state.entries['to'] || this.state.entries['receiver'] || this.state.entries['rcvr']}</strong>`);
                    this.props.toggleDisplayCall();
            }).catch(error => {
                console.log(error);
                this.setState({errorCall: 'Call could not be processed', pending: false, ready: false});
            });
    }).catch(error => {
        console.log(error);
        this.setState({error: 'Invalid password', pending: false, ready: false});
    });
  }

  sendLedgerOperation = () => {
    const keystore = JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}'? this.state.encryptedWallet : this.props.userData.keystore[this.props.indexer];
    this.setState({pending: true});
    console.log(`Sending ${this.state.parameters.entrypoint} operation to ${this.props.selectedToken.contract_hash} with fees to ${this.state.feesTez} and burn cap to ${this.state.burnTez}`);
    this._asyncRequest = tezos.TezosNodeWriter.sendContractInvocationOperation(this.props.tezosNodes[0], this.state.ledgerKeystore, this.props.selectedToken.contract_hash, 0, this.state.feesTez, derivationPath, this.state.storageCost, this.state.burnTez, this.state.parameters.entrypoint, this.state.parameters.parameters, tezos.TezosParameterFormat.Michelson).then(
        operation => {
            this.setState({pending: false});
            this.props.renderSuccessMsg(`Call for <strong>${this.props.feature.entrypoint} ${this.props.selectedToken.token_symbol}</strong> sent with ID <small>${operation.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.state.entries['amount'] || this.state.entries['value']} ${this.props.selectedToken.token_symbol}</strong> to <strong>${this.state.entries['to'] || this.state.entries['receiver'] || this.state.entries['rcvr']}</strong>`);
            this.props.toggleDisplayCall();
    }).catch(error => {
        console.log(error);
        this.setState({errorCall: 'Call could not be processed', pending: false, ready: false});
    });
  }

  render() {
    return (
        <Row className="card-operation">
            <Col md="3" className="d-none d-md-block"></Col>
            <Col xs="12" md="6">
                <Card className="card-stats card-blur mt-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h2"
                            className="text-uppercase text-secondary mb-0"
                          >
                            {this.props.feature.display} {this.props.selectedToken.token_symbol}
                          </CardTitle>
                          <span className="text-white d-block text-sm">Available funds :
                              <span className="font-weight-bold mb-0 text-yellow shadow-sm ml-2 text-md mr-2">
                                {this.props.balance} ꜩ
                              </span>
                              |
                              <span className="font-weight-bold mb-0 text-yellow shadow-sm ml-2 text-md">
                                {this.props.selectedToken.value && this.props.selectedToken.value.balance} {this.props.selectedToken.token_symbol}
                              </span>
                          </span>
                          <hr/>
                          {this.props.userData && this.props.userData.value[this.props.indexer] &&
                            <span className="text-white d-block text-sm">Sending from <span className="text-yellow">{this.props.userData.value[this.props.indexer]}</span></span>
                          }
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-primary text-white rounded-circle shadow">
                            <i className="fas fa-exchange-alt"></i>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                      {this.state.error && <Col xs="12"><span className="text-danger d-block ml-2 mt-2">
                           <strong>{this.state.error}</strong>
                        </span></Col>}
                        <Col md="12">
                            <p className="mt-3 mb-1 text-secondary text-sm">Contract address</p>
                            <Input type="text" placeholder="tz1/KT1 receiver" value={this.props.selectedToken.contract_hash} className="ml-1 border-0" disabled/>
                        </Col>
                        {this.state.ready?
                            <>
                                {this.props.feature.values.map(entry =>
                                    <Col md="12" key={`var_${entry.name}`}>
                                        <p className="mt-3 mb-1 text-secondary text-sm">
                                            {entry.description}:
                                            <span className="ml-2 text-yellow">{this.state.entries[entry.name]}</span>
                                        </p>
                                    </Col>
                                )}
                                <Col md="12">
                                    <hr/>
                                    <p className="mt-3 mb-1 text-secondary text-sm">
                                        Baker fees:
                                        <span className="ml-2 text-yellow">{parseFloat(this.state.feesTez/1000000)} ꜩ</span>
                                    </p>
                                </Col>
                            </>
                        :
                            <>
                                {this.props.feature.values.map(entry =>
                                    <Col md="12" key={entry.name}>
                                        <p className="mt-3 mb-1 text-secondary text-sm">{entry.description}</p>
                                        <Input type={entry.type === "text"? "text" : "number"}
                                               placeholder={entry.description}
                                               className="ml-1 border-0"
                                               autoComplete="off"
                                               onChange={(e) => this.updateEntry(e, entry.name)}/>
                                    </Col>
                                )}
                                {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' &&
                                    <Col md="12">
                                        <p className="mt-3 mb-1 text-secondary text-sm">Password</p>
                                        <Input type="password" placeholder="Password to unlock keystore file" className="ml-1 border-0"
                                        onChange={(e) => this.updatePassword(e)}/>
                                    </Col>
                                }
                                {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' && JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}' &&
                                    <Col md="12" className="mt-4">
                                        <input
                                              placeholder="Keystore file"
                                              type="file" className="d-none"
                                              ref={this.hiddenFileInput}
                                              onChange={(e) => this.onFileHandler(e)}
                                        />
                                        {!this.state.encryptedWallet?
                                              <Button color="primary" type="button" onClick={this.handleUpload}>
                                                  Upload Keystore file
                                              </Button>
                                        :
                                             <div>
                                                <p className="text-yellow mb-1">
                                                    <small><strong>{this.state.fileName}</strong> selected</small>
                                                </p>
                                                <p className="text-info cursor-pointer mt-1" onClick={() => {this.setState({encryptedWallet: null})}}>
                                                    <small>Click to change file</small>
                                                </p>
                                             </div>
                                        }
                                    </Col>
                                }
                            </>
                        }
                        {this.state.ledgerKeystore && <span className="text-yellow d-block ml-2 mt-2">
                           <small>{this.state.ledgerKeystore.publicKeyHash} connected</small>
                        </span>}
                      </Row>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'ledger' && !this.state.ledgerKeystore && !this.state.loading &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2"
                                              onClick={this.unlockLedger}>
                                        Connect Ledger
                            </Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.loading &&
                            <small className="text-white">Follow instructions on your Ledger...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.ledgerKeystore && !this.state.pending && !this.state.ready &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.ledgerSendOp()}}>{this.props.feature.display} {this.props.selectedToken.token_symbol}</Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.ledgerKeystore && !this.state.pending && this.state.ready &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.sendLedgerOperation()}}>Confirm operation</Button>
                        }
                        {this.state.pending && this.props.userData.type[this.props.indexer] === 'file' &&
                            <small className="text-secondary">Processing...</small>
                        }
                        {this.state.pending && this.props.userData.type[this.props.indexer] === 'ledger' &&
                            <small className="text-secondary">Validate operation on your Ledger...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'file' && !this.state.pending && !this.state.ready &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.keystoreSimulateOp()}}>{this.props.feature.display} {this.props.selectedToken.token_symbol}</Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'file' && !this.state.pending && this.state.ready &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.sendKeystoreOperation()}}>Confirm operation</Button>
                        }

                        <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right"  onClick={() => {this.props.toggleDisplayCall()}}>Cancel</Button>
                    </CardFooter>
                  </Card>
            </Col>
            <Col md="3" className="d-none d-md-block"></Col>
        </Row>
    );
  }
}

export default Call;
