import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  Col,
  Input,
  Row,
} from "reactstrap";
// layout for this page
import * as tezos from 'conseiljs';
import {storeAuth} from '../../../utils/user.js';

const derivationPath = "44'/1729'/0'/0'/0'";

class Transfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pending: false,
      error: null,
      ledgerKeystore: null,
      receiver: null,
      password: null,
      xtzAmount: null,
      encryptedWallet: null
    };
    this.hiddenFileInput = React.createRef();
  };

  updateReceiver = (e) => {
    this.setState({receiver: e.target.value});
  }

  updateAmount = (e) => {
    this.setState({xtzAmount: e.target.value});
  }

  updatePassword = (e) => {
    this.setState({password: e.target.value});
    if(this.props.donate && !this.state.receiver)
      this.setState({receiver: this.props.donateAddress})
  }

  unlockLedger = async () => {
        this.setState({loading: true, error: null});
        try{
            const keystore = await tezos.TezosLedgerWallet.unlockAddress(0, derivationPath);
            this.setState({loading: false, ledgerKeystore: keystore, error: null});
        }catch(error){
            this.setState({loading: false, error: 'Could not connect Ledger device'});
        }
        if(this.props.donate)
          this.setState({receiver: this.props.donateAddress})
  }

  handleUpload = (event) => {
    this.hiddenFileInput.current.click();
  };

  onFileHandler = (event) => {
    const reader = new FileReader();
    reader.addEventListener('load', event => {
        try{
            const data = event.target.result.split(';base64,').pop();
            const buff = new Buffer(data, 'base64');
            const jsonData = JSON.parse(buff.toString('ascii'));
            this.setState({
                encryptedWallet: jsonData,
                errorFile: false
            });
            const keystoreAuth = {
                type: ["file"],
                value: [this.props.userData.value[this.props.indexer]],
                keystore: [jsonData]
            }
            storeAuth(keystoreAuth);
        }catch(error){
            console.log(error);
            this.setState({errorFile: true});
        }
    });
    this.setState({fileName: event.target.files[0].name});
    reader.readAsDataURL(event.target.files[0]);
  }

  ledgerSendOp= () => {
        if(!this.state.ledgerKeystore)
            this.setState({error: 'Please connect your ledger first'});
        else if(!this.state.xtzAmount || !this.state.receiver)
            this.setState({error: 'Missing parameters'});
        else{
            this.setState({pending: true, error: null});
            this._asyncRequest = tezos.TezosNodeWriter.sendTransactionOperation(this.props.tezosNodes[0], this.state.ledgerKeystore, this.state.receiver, Math.round(parseFloat(this.state.xtzAmount) * 1000000), 2000, derivationPath).then(result => {
                this.setState({pending: false});
                this.props.renderSuccessMsg(`Transfer of <strong>${this.state.xtzAmount} ꜩ</strong> sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> to <strong>${this.state.receiver}</strong>`);
                this.props.toggleDisplayTransfer()
            })
            .catch(error => {
                this.setState({error: error, pending: false});
            });
        }
    }

  keystoreSendOp= () => {
        if(!this.props.userData.type[this.props.indexer] === 'file')
            this.setState({error: 'Please select a Keystore wallet file'});
        else if(!this.state.xtzAmount || !this.state.receiver || !this.state.password || (JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}' && ! this.state.encryptedWallet))
            this.setState({error: 'Missing parameters'});
        else{
            this.setState({pending: true, error: null});
            const keystore = JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}'? this.state.encryptedWallet : this.props.userData.keystore[this.props.indexer];
            this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(keystore), this.state.password).then(loadedWallet => {
                this._asyncRequest = tezos.TezosNodeWriter.sendTransactionOperation(this.props.tezosNodes[0], loadedWallet.identities[0], this.state.receiver, Math.round(parseFloat(this.state.xtzAmount) * 1000000), 2000, derivationPath).then(result => {
                    this.setState({pending: false});
                    this.props.renderSuccessMsg(`Transfer of <strong>${this.state.xtzAmount} ꜩ</strong> sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> to <strong>${this.state.receiver}</strong>`);
                    this.props.toggleDisplayTransfer()
                })
                .catch(error => {
                    this.setState({error: error, pending: false});
                });
            })
            .catch(error => {
                this.setState({error: 'Invalid password', pending: false});
            });
        }
    }

  render() {
    return (
        <Row className="card-operation">
            <Col md="3" className="d-none d-md-block"></Col>
            <Col xs="12" md="6">
                <Card className="card-stats card-blur mt-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h2"
                            className="text-uppercase text-secondary mb-0"
                          >
                            {this.props.donate? 'Donate' : 'Transfer ꜩ'}
                          </CardTitle>
                          <span className="text-white d-block text-sm">Available funds :
                              <span className="font-weight-bold mb-0 text-yellow shadow-sm ml-2 text-md">
                                {this.props.balance} ꜩ
                              </span>
                          </span>
                          <hr/>
                          {this.props.userData && this.props.userData.value[this.props.indexer] &&
                            <span className="text-white d-block text-sm">Sending from <span className="text-yellow">{this.props.userData.value[this.props.indexer]}</span></span>
                          }
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                            <i className="fas fa-exchange-alt"></i>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                      {this.state.error && <Col xs="12"><span className="text-danger d-block ml-2 mt-2">
                           <strong>{this.state.error}</strong>
                        </span></Col>}
                        <Col xs="12" md="6">
                            <p className="mt-3 mb-1 text-secondary text-sm">Amount in ꜩ</p>
                            <Input type="number" placeholder="Amount in ꜩ" className="ml-1 border-0"
                            onChange={(e) => this.updateAmount(e)}/>
                        </Col>
                        <Col md="12">
                            <p className="mt-3 mb-1 text-secondary text-sm">Receiver</p>
                            {this.props.donate?
                              <small className="d-block text-white mt-2 mb-3"><strong className="text-primary mr-1">tezOrus</strong> ({this.props.donateAddress})</small>
                            :
                              <Input type="text" placeholder="tz1/KT1 receiver" className="ml-1 border-0"
                              onChange={(e) => this.updateReceiver(e)}/>
                            }
                        </Col>
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' &&
                            <Col md="12">
                                <p className="mt-3 mb-1 text-secondary text-sm">Password</p>
                                <Input type="password" placeholder="Password to unlock keystore file" className="ml-1 border-0"
                                onChange={(e) => this.updatePassword(e)}/>
                            </Col>
                        }
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' && JSON.stringify(this.props.userData.keystore[this.props.indexer]) === '{}' &&
                            <Col md="12" className="mt-4">
                                <input
                                      placeholder="Keystore file"
                                      type="file" className="d-none"
                                      ref={this.hiddenFileInput}
                                      onChange={(e) => this.onFileHandler(e)}
                                />
                                {!this.state.encryptedWallet?
                                      <Button color="primary" type="button" onClick={this.handleUpload}>
                                          Upload Keystore file
                                      </Button>
                                :
                                     <div>
                                        <p className="text-yellow mb-1">
                                            <small><strong>{this.state.fileName}</strong> selected</small>
                                        </p>
                                        <p className="text-info cursor-pointer mt-1" onClick={() => {this.setState({encryptedWallet: null})}}>
                                            <small>Click to change file</small>
                                        </p>
                                     </div>
                                }
                            </Col>
                        }
                        {this.state.ledgerKeystore && <span className="text-yellow d-block ml-2 mt-2">
                           <small>{this.state.ledgerKeystore.publicKeyHash} connected</small>
                        </span>}
                      </Row>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'ledger' && !this.state.ledgerKeystore && !this.state.loading &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2"
                                              onClick={this.unlockLedger}>
                                        Connect Ledger
                            </Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.loading &&
                            <small className="text-white">Follow instructions on your Ledger...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.ledgerKeystore && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.ledgerSendOp()}}>{this.props.donate? 'Donate' : 'Transfer ꜩ'}</Button>
                        }
                        {this.state.pending &&
                            <small className="text-secondary">Sending transfer operation...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'file' && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.keystoreSendOp()}}>{this.props.donate? 'Donate' : 'Transfer ꜩ'}</Button>
                        }

                        <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right"  onClick={() => {this.props.toggleDisplayTransfer()}}>Cancel</Button>
                    </CardFooter>
                  </Card>
            </Col>
            <Col md="3" className="d-none d-md-block"></Col>
        </Row>
    );
  }
}

export default Transfer;
