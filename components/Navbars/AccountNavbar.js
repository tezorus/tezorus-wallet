import React from "react";
import Link from "next/link";
import axios from "axios";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  NavbarBrand,
  Nav,
  Container,
  Media,
} from "reactstrap";
import {loadAuth, logOut} from '../../utils/user.js';
import logo from '../../assets/img/brand/logo-white.png';
import {getJWT} from '../../utils/user.js';

class AccountNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        userData: null,
        searchData: null,
        type: null,
        jwt: null
    };
  }

  componentDidMount = () => {
    document.body.classList.remove("bg-default");
    const userData = loadAuth();
    if(userData)
        this.setState({userData: userData});
    const jwt = getJWT();
    this.setState({jwt: jwt});
  };

  updateSearchData = (e) => {
    this.setState({searchData: e.target.value});
  }

  searchData = (e) => {
    this.setState({searchData: e.target.value, type: null});
    if(e.keyCode === 13){
        console.log(`Searching for ${e.target.value}`)
        if((e.target.value.startsWith("tz1") || e.target.value.startsWith("KT1")) && e.target.value.length === 36){
            this._asyncRequest = axios.get(`${this.props.expFabrick}account/${e.target.value}`).then(
                result => {
                    this.setState({type: 'address'});
                }
            ).catch(error => {
                console.log(error);
                this.setState({type: 'not found'});
            })
        }else if(e.target.value.startsWith("o") && e.target.value.length === 51){
            this._asyncRequest = axios.get(`${this.props.expFabrick}operation/${e.target.value}`).then(
                result => {
                    this.setState({type: 'operation'});
                }
            ).catch(error => {
                console.log(error);
                this.setState({type: 'not found'});
            })
        }else
            this.setState({type: 'error'});
    }
  }

  render() {
    return (
      <>
        <Navbar expand="md" id="navbar-main">
          <Container fluid>
            <NavbarBrand href="#pablo" className="pt-0 d-md-flex">
                <Link href={`/account/dashboard?network=${this.props.network}&indexer=${this.props.indexer}`}>
                    <img alt="logo-fabrick" src={logo} className="main-logo" />
                </Link>
            </NavbarBrand>
            <small className="d-block text-white bg-dark m-3 p-1 rounded" style={{maxWidth: '500px'}}>Currently in BETA version. Use for trial only. 
              <br/>Please contact us on <a href="https://t.me/tezorus" target="_blank">Telegram</a> or <a href="https://discord.gg/bcevXG8CvU" target="_blank">Discord</a> for bug reports.</small>
            <div className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <InputGroup className="input-group-alternative mb-0">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fas fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Search" type="text"
                         onKeyDown={(e) => this.searchData(e)}/>
                </InputGroup>
            </div>
            {this.state.type &&
                <>
                    {this.state.type === 'error'?
                        <div className="bg-danger p-2 rounded text-sm d-block mb-2 float-right mr-2">
                            <span className="cursor-pointer float-right mr-2 ml-2 text-white cursor-pointer" onClick={() => {this.setState({type: null})}}>
                                <i className="far fa-times-circle"></i>
                            </span>
                            <span className="text-white">
                                Invalid format
                            </span>
                        </div>
                    :
                        this.state.type === 'not found'?
                            <div className="bg-danger p-2 rounded text-sm d-block mb-2 float-right mr-2">
                                <span className="cursor-pointer float-right mr-2 ml-2 text-white cursor-pointer" onClick={() => {this.setState({type: null})}}>
                                    <i className="far fa-times-circle"></i>
                                </span>
                                <span className="text-white">
                                    Not found
                                </span>
                            </div>
                        :
                            this.state.type === 'address'?
                                <div className="bg-yellow p-2 rounded text-sm d-block mb-2 float-right mr-2 cursor-pointer"
                                     onClick={() => { window.open(`/account/address?address=${this.state.searchData}&network=${this.props.network}`)}}>
                                    <span className="float-right mr-2 ml-2 text-black cursor-pointer">
                                        <i className="fas fa-chevron-right"></i>
                                    </span>
                                    <span className="text-black cursor-pointer">
                                        1 {this.state.type} found
                                    </span>
                                    <small className="d-block text-muted">Click to see details</small>
                                </div>
                            :
                                <div className="bg-yellow p-2 rounded text-sm d-block mb-2 float-right mr-2 cursor-pointer"
                                     onClick={() => { window.open(`/account/operation?op=${this.state.searchData}&network=${this.props.network}`)}}>
                                    <span className="float-right mr-2 ml-2 text-black cursor-pointer">
                                        <i className="fas fa-chevron-right"></i>
                                    </span>
                                    <span className="text-black cursor-pointer">
                                        1 {this.state.type} found
                                    </span>
                                    <small className="d-block text-muted">Click to see details</small>
                                </div>
                    }
                </>
            }
            <Nav className="align-items-center d-none d-flex" navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle className="pr-0" nav>
                  <Media className="align-items-center">
                    <span className="avatar avatar-lg">
                      <img
                        alt="..."
                        src={require("assets/img/theme/avatar.png")}
                      />
                    </span>
                    <Media className="ml-2 d-none d-lg-block">
                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-arrow" right>
                  <DropdownItem className="noti-title" header tag="div">
                    <h6 className="text-overflow m-0">Welcome!</h6>
                  </DropdownItem>
                  {!this.state.jwt && <Link href="/auth/login">
                    <DropdownItem>
                      <span>Login</span>
                    </DropdownItem>
                  </Link>}
                  {!this.state.jwt && <Link href="/auth/register">
                    <DropdownItem>
                      <span>Register</span>
                    </DropdownItem>
                  </Link>}
                  <DropdownItem divider />
                  {this.state.jwt && 
                    <DropdownItem
                    onClick={() => {location.href='/account/settings'}}
                    >
                      <i className="ni ni-settings" />
                      <span>Settings</span>
                    </DropdownItem>
                  }
                  <DropdownItem
                    onClick={() => {logOut()}}
                  >
                    <i className="ni ni-user-run" />
                    <span>Quit</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default AccountNavbar;
