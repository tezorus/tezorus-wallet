import React from "react";
import axios from "axios";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Table,
  Container,
  Row,
} from "reactstrap";
// layout for this page
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import {truncStringPortion} from '../../utils/helper.js';

import { FABRICK_EXP } from '../../utils/constants.js';

class Operation extends React.Component {
  static getInitialProps ({ query: { op, network } }) {
    return { opHash: op, network: network }
  }

  constructor(props) {
    super(props);
    this.state = {
      network: "mainnet",
      expFabrick: `${FABRICK_EXP}mainnet/`,
      fetching: true,
      operations: [],
      loadedContracts: [],
    };
  };

  componentDidMount = () => {
    this.setState({network: this.props.network,
                   expFabrick: `${FABRICK_EXP}${this.props.network}/`});
    setTimeout(
        function() {
            this.fetchOperation(this.props.opHash);
        }
        .bind(this),
        500
    );
  };

  fetchOperation = (opHash) => {
    console.log(`${this.state.expFabrick}operation/${opHash}`);
    this._asyncRequest = axios.get(`${this.state.expFabrick}operation/${opHash}`).then(
        res => {
            if(res.data){
                let ops = [];
                res.data.operation.forEach(op => {
                    if(op.parameters)
                        op.parameters = op.parameters.value[op.parameters.entrypoint] || op.parameters.value
                    ops.push(op);
                });
                this.setState({operations: ops,
                   fetching: false});
            }
        }
    ).catch(error => {
        console.log(error);
        this.setState({operations: [], fetching: false});
    });
  }

  copyElement = (element) => {
    navigator.clipboard.writeText(element);
  }

  render() {
    return (
      <div className="main-content">
          <AccountNavbar expFabrick={this.state.expFabrick} />
          <Container className="mt-6" fluid style={{minHeight: '70vh'}}>
              <Row>
                <div className="col">
                  <Card className="card-blur shadow">
                    <CardHeader className="border-0 bg-transparent">
                      <h3 className="mb-2 text-white d-inline">
                        Operation <small className="text-yellow">{this.props.opHash}</small>
                        <span className="ml-2 mr-2">|</span>
                        Network <small className="text-yellow">{this.state.network}</small>
                      </h3>
                    </CardHeader>
                    {this.state.operations &&
                        <Table className="align-items-center table-flush text-secondary mb-4" responsive>
                          <thead className="thead-dark">
                            <tr>
                              <th scope="col" className="bg-transparent">Op hash</th>
                              <th scope="col" className="bg-transparent">Type</th>
                              <th scope="col" className="bg-transparent">Date</th>
                              <th scope="col" className="bg-transparent">Amount</th>
                              <th scope="col" className="bg-transparent">Status</th>
                              <th scope="col" className="bg-transparent">Parameters</th>
                              <th scope="col" className="bg-transparent" />
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.operations && this.state.operations.length > 0
                                && this.state.operations.map((op, index) => (
                                <tr key={`${op.hash}-${index}`}>
                                  <th scope="row">
                                    {truncStringPortion(op.hash, 7, 6)}
                                  </th>
                                  <td>
                                    {op.parameters?
                                        <Badge color={'dark'} size="md">
                                          <span className="text-white">{op.parameters.entrypoint}</span>
                                          <small className="d-block text-white mt-2">Smart contract call</small>
                                        </Badge>
                                    :
                                        <Badge color={'primary'}>
                                          <span className="text-white">{op.type === 'transaction'? 'transfer ꜩ' : op.type}</span>
                                        </Badge>
                                    }
                                  </td>
                                  <td>
                                    {new Date(op.time).toLocaleDateString(navigator.userLanguage, {hour: '2-digit', minute:'2-digit'})}
                                  </td>
                                  <td>
                                    <h4 className="text-yellow">{op.volume} ꜩ</h4>
                                    <small className="d-block">Baker fees: {op.fee} ꜩ</small>
                                    {op.burned > 0 && <small className="d-block">Gas burnt: {op.burned} ꜩ</small>}
                                  </td>
                                  <td>
                                    {op.status === 'applied'?
                                        <Badge color="" className="badge-dot text-success">
                                          <i className="bg-success"/>
                                          <small>{op.status}</small>
                                        </Badge>
                                    :
                                        <Badge color="" className="badge-dot text-danger">
                                          <i className="bg-danger"/>
                                          <small>{op.status}</small>
                                        </Badge>
                                    }
                                  </td>
                                  <td>
                                    <span className="mb-0 d-block">
                                          <small>
                                            caller:
                                            <span className="text-secondary fw-semi-bold">
                                              &nbsp; {op.sender}
                                            </span>
                                          </small>
                                    </span>
                                    <span className="mb-0 d-block">
                                          <small>
                                            receiver:
                                            <span className="text-secondary fw-semi-bold">
                                              &nbsp; {op.receiver}
                                            </span>
                                          </small>
                                    </span>
                                    {op.parameters && <div>
                                        {typeof op.parameters !== 'string'?
                                            Object.keys(op.parameters).map((e, i) => op.parameters[e] &&
                                                <span className="mb-0 d-block" key={e}>
                                                      <small>
                                                        {e}:
                                                        <span className="text-secondary fw-semi-bold">
                                                          &nbsp; {op.parameters[e].toString().length > 36? truncStringPortion(op.parameters[e].toString(), 12, 12) : op.parameters[e].toString()}
                                                        </span>
                                                      </small>
                                                </span>
                                            )
                                        :
                                            op.parameters &&
                                            <span className="mb-0 d-block">
                                                  <small>
                                                    parameter:
                                                    <span className="text-secondary fw-semi-bold">
                                                      &nbsp; {op.parameters.length > 36? truncStringPortion(op.parameters.toString, 12, 12) : op.parameters}
                                                    </span>
                                                  </small>
                                            </span>
                                        }
                                    </div>}
                                  </td>
                                  <td className="text-right">
                                    <UncontrolledDropdown>
                                      <DropdownToggle
                                        className="btn-icon-only text-light"
                                        href="#pablo"
                                        role="button"
                                        size="sm"
                                        color=""
                                        onClick={(e) => e.preventDefault()}
                                      >
                                        <i className="fas fa-ellipsis-v" />
                                      </DropdownToggle>
                                      <DropdownMenu className="dropdown-menu-arrow" right>
                                        <DropdownItem
                                          href="#pablo"
                                          onClick={() => this.props.copyElement(op.hash)}
                                        >
                                          Copy operation hash / Id
                                        </DropdownItem>
                                        <DropdownItem
                                          href={`${this.state.expFabrick}operation/${op.hash}`}
                                          target="_blank"
                                        >
                                          Full stack
                                        </DropdownItem>
                                      </DropdownMenu>
                                    </UncontrolledDropdown>
                                  </td>
                                </tr>
                            ))}
                          </tbody>
                        </Table>
                    }
                    {!this.state.fetching && this.state.transactions && this.state.transactions.length === 0 &&
                        <p className="text-secondary ml-3 mt-2">No transactions</p>}
                    {this.state.fetching && <span className="h2 font-weight-bold mb-0 text-info ml-4 mb-4">
                            <i className="fas fa-circle-notch fa-spin"></i>
                        </span>}
                  </Card>
                </div>
              </Row>
          </Container>
          <AccountFooter />
      </div>
    );
  }
}

export default Operation;
