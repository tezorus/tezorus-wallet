import React from "react";
import axios from "axios";
import Link from "next/link";

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import Auth from "layouts/Auth.js";

const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})");
const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");
const weakRegex = new RegExp("^(?=.{4,})");
const emailRegex = /\S+@\S+\.\S+/;

class ForgottenPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      token: null,
      expFabrick: 'https://explorer-test.fabrick.tech/api/v1/account/',
      error: null,
      success: false,
      newPassword: false,
      pending: false,
      jwtoken: null
    };
  };

  updateUsername = (e) => {
    this.setState({username: e.target.value});
  }

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  updateToken = (e) => {
    this.setState({token: e.target.value});
  }

  sendToken = () => {
    this.setState({error: null, success: false});
    if(!emailRegex.test(this.state.username)){
        this.setState({error: 'Invalid Email'});
    }else{
        this.setState({pending: true});
        this._asyncRequest = axios.post(`${this.state.expFabrick}password/reset/token`, {
                                email_address: this.state.username
                            }
                        ).then(
            res => {
                console.log(res);
                if(res.data && res.data.status){
                    this.setState({success: true, pending:false});
                }else if(res.data && !res.data.status){
                    this.setState({error: res.data.error, pending:false});
                }else{
                    this.setState({error: 'Email could not be send', pending:false});
                }
            }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not send email', pending: false});
        });
    }
  }

  resetPassword = () => {
    this.setState({error: null, newPassword: false});
    if(!mediumRegex.test(this.state.password) && !this.state.token){
        this.setState({error: 'Invalid parameters'});
    }else{
        this.setState({pending: true});
        this._asyncRequest = axios.post(`${this.state.expFabrick}password/reset`, {
                                password: this.state.password,
                                reset_token: this.state.token,
                                email_address: this.state.username
                            }
                        ).then(
            res => {
                console.log(res);
                if(res.data && res.data.status){
                    this.setState({success: false, pending:false, newPassword: true});
                }else if(res.data && !res.data.status){
                    this.setState({error: res.data.error, pending:false});
                }else{
                    this.setState({error: 'Incorrect token', pending:false});
                }
            }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not reset password', pending: false});
        });
    }
  }

  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="shadow border-0 card-blur">
            {this.state.newPassword?
                <CardBody className="px-lg-5 py-lg-5 text-secondary">
                    <div className="text-center mb-4">
                        <h1 className="text-white"><big><i className="far fa-check-circle"></i></big></h1>
                        <h2 className="text-yellow">Reset successful</h2>
                        <p>Your new password is now effective.
                          <a
                            className="text-info ml-1 mr-1"
                            href="/auth/login"
                          >
                            <strong>Login</strong>
                          </a>now</p>
                      </div>
                </CardBody>
            :
                <CardBody className="px-lg-5 py-lg-5 text-secondary">
                  <div className="text-center mb-4">
                    <h1 className="text-white"><big><i className="far fa-question-circle"></i></big></h1>
                    <h2 className="text-yellow">Forgotten Password</h2>
                    <p>Set your email to receive a reset Password token</p>
                  </div>
                  {this.state.error && <p className="text-danger">{this.state.error}</p>}
                  {this.state.success && <p className="text-success">Reset Email sent to {this.state.username}</p>}
                  <Form role="form">
                  {this.state.success?
                     <div>
                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <Input
                              placeholder="Token"
                              type="text"
                              onChange={this.updateToken}
                            />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <Input
                              placeholder="Password"
                              type="password"
                              onChange={this.updatePassword}
                            />
                          </InputGroup>
                        </FormGroup>
                        {this.state.password && <div className="text-center">
                            {strongRegex.test(this.state.password)?
                                <Badge color={'success'} className="text-white">
                                  Strong
                                  <small className="d-block">password</small>
                                </Badge>
                            :
                                mediumRegex.test(this.state.password)?
                                    <Badge color={'warning'} className="text-white">
                                      Medium
                                      <small className="d-block">password</small>
                                    </Badge>
                                :
                                     weakRegex.test(this.state.password) &&
                                         <Badge color={'danger'} className="text-white">
                                            Weak
                                            <small className="d-block">password</small>
                                         </Badge>
                            }
                        </div>}
                     </div>
                  :
                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <Input
                              placeholder="Email"
                              type="email"
                              autoComplete="new-email"
                              onChange={this.updateUsername}
                            />
                          </InputGroup>
                        </FormGroup>
                  }
                  </Form>
                  <div className="text-center">
                      {this.state.pending?
                            <span className="h2 font-weight-bold mb-0 text-info ml-4 mb-4">
                                <i className="fas fa-circle-notch fa-spin"></i>
                            </span>
                      :
                          <>
                              {this.state.success?
                                  <Button className="mt-4 float-left" color="primary" type="button" onClick={() => this.resetPassword()}>
                                    Reset password
                                  </Button>
                              :
                                  <Button className="mt-4 float-left" color="primary" type="button" onClick={() => this.sendToken()}>
                                    Send token
                                  </Button>
                              }
                              <Link href="/auth/home"><Button className="mt-4 float-right" color="secondary" type="button">
                                Cancel
                              </Button></Link>
                          </>
                      }
                  </div>
                </CardBody>
            }
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="/auth/login"
              >
                <small>Login</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="/auth/register"
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

ForgottenPassword.layout = Auth;

export default ForgottenPassword;
