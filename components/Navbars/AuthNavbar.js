import React from "react";
import Link from "next/link";
// reactstrap components
import {
  NavbarBrand,
  Navbar,
  Container,
} from "reactstrap";

class AuthNavbar extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
        };
  }

  render() {
    return (
      <>
        <Navbar
          className="navbar-top navbar-horizontal navbar-dark"
          expand="md"
        >
          <Container className="px-4">
            <Link href="/account/dashboard">
              <span>
                <NavbarBrand href="#pablo">
                  <img
                    alt="..."
                    src={require("assets/img/brand/logo-white.png")}
                  />
                  <small className="d-block text-white bg-dark mt-3 mb-3 p-1 rounded">Currently in BETA version. Use for trial only. 
                  <br/>Please contact us on <a href="https://t.me/tezorus" target="_blank">Telegram</a> or <a href="https://discord.gg/bcevXG8CvU" target="_blank">Discord</a> for bug reports.</small>
                </NavbarBrand>
              </span>
            </Link>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default AuthNavbar;
