import React from "react";
import axios from "axios";
// reactstrap components
import { Container} from "reactstrap";
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import Router from "next/router";
import {loadAuth, selectWallet, getJWT, rmAuth} from '../../utils/user.js';

import Header from "components/Headers/Header.js";
import Operations from "./components/operations.js";
import Transfer from "./components/transfer.js";
import Reveal from "./components/reveal.js";
import Delegate from "./components/delegate.js";
import Call from "./components/call.js";

import { FABRICK_EXP } from '../../utils/constants.js';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      indexer: 0,
      userData: null,
      network: "mainnet",
      accountDetails: null,
      tokens: [],
      expFabrick: `${FABRICK_EXP}mainnet/`,
      transactions: null,
      displayMore: false,
      fetching: true,
      counter: 1,
      displayTransfer: false,
      displayReveal: false,
      displayDelegate: false,
      displayCall: false,
      tezosNodes: ['https://mainnet.smartpy.io/'],
      successMsg: null,
      blockHead: 0,
      cycle: 0,
      feature: {},
      selectedToken: {},
      fetchOps: 'xtz',
      networks: ['mainnet'],
      tezEur: 0,
      totalBalance: 0,
      expAPI: null,
      contractHash: null,
      maxWallets: 5,
      donate: false,
      donateAddress: null,
    };
  };

  componentDidMount = () => {
    const userData = loadAuth();
    if(userData){
        const queries = new URLSearchParams(window.location.search);
        this.setState({userData: userData,
                       network: queries.get('network') || 'mainnet',
                       indexer: queries.get('indexer') || 0,
                       expFabrick: `${FABRICK_EXP}${queries.get('network') || 'mainnet'}/`});
        setTimeout(
            function() {
                this.fetchAccountDetails();
            }
            .bind(this),
            500
        );
    }else
        Router.push("/auth/home");
  };

  fetchAccountDetails = () => {
    //Get XTZ-Eur change based on coinmarketcap
    this._asyncRequest = axios.get(`${FABRICK_EXP}token_info`).then(response => {
        this.setState({tezEur: response.data.token_info.price.eur});
    })
    .catch(error => {
        console.log(error);
    })
    this.setState({accountDetails: null,
                   tokens: [],
                   fetching: true,
                   transactions: [],
                   successMsg: null,
                   displayMore: false,
                   fetchOps: 'xtz',
                   contractHash: null,
                   totalBalance: 0});
    this.state.userData.value.forEach((wallet, index) => {
        this._asyncRequest = axios.get(`${this.state.expFabrick}balance/${wallet}`).then(
            result => {
                if(result.data.status)
                    this.setState({totalBalance: (Math.round((this.state.totalBalance + result.data.spendable_balance) * 1000000) / 1000000)});
        })
        .catch(error => {
            console.log(error);
        });
    });
    //Get account details
    this._asyncRequest = axios.get(`${this.state.expFabrick}account/${this.state.userData.value[this.state.indexer]}`).then(
        result => {
            if(result.data && result.data.status && result.data.account){
                let ops = [];
                result.data.account.ops.forEach(op => {
                    if(op.parameters){
                        op.entrypoint = op.parameters.entrypoint;
                        op.parameters = op.parameters.value[op.parameters.entrypoint] || op.parameters.value
                    }
                    ops.push(op);
                });
                this.setState({accountDetails: result.data.account,
                               transactions: ops,
                               fetching: false,
                               counter: 1,
                               displayMore: ops.length === 10? true : false});
            }else
                this.setState({accountDetails: {
                    spendable_balance: 0,
                    n_ops: 0,
                    is_revealed: false,
                    total_received: 0,
                    total_sent: 0,
                    total_burned: 0,
                    total_fees_paid: 0}, transactions: [], fetching: false});
        }
    ).catch(error => {
        console.log(error);
        this.setState({accountDetails: {
                    spendable_balance: 0,
                    n_ops: 0,
                    is_revealed: false,
                    total_received: 0,
                    total_sent: 0,
                    total_burned: 0,
                    total_fees_paid: 0}, transactions: [], fetching: false});
    });
    //Fetch config; with FA tokens
    this._asyncRequest = axios.get(`${this.state.expFabrick}tezorus/config`).then(response => {
        response.data.tokens.forEach(token => {
            this.fetchTokenDetails(token);
        });
        this.setState({networks: response.data.tezos_networks, 
                       expAPI: response.data.explorer[0].api, 
                       maxWallets: response.data.app_settings.max_wallets,
                       donateAddress: response.data.donate_address});
    })
    .catch(error => {
        console.log(error);
    })
    //Fetch block head and cycle number
    this._asyncRequest = axios.get(`${this.state.expFabrick}block/head`).then(response => {
        this.setState({blockHead: response.data.block_number, cycle: response.data.block_data.cycle});
    })
    .catch(error => {
        console.log(error);
    })
  }

  fetchTokenDetails = (fetchToken) => {
    axios.get(`${this.state.expFabrick}balance/${this.state.userData.value[this.state.indexer]}/${fetchToken.contract_hash}`).then(response => {
        if(response.data.status){
            let details = fetchToken;
            details.value = {balance: parseFloat(response.data.balance)};
            this.setState({
                tokens: [
                ...this.state.tokens,
                details
                ]
            });
        }else{
            let details = fetchToken;
            details.value = {balance: 0};
            this.setState({
                tokens: [
                    ...this.state.tokens,
                    details
                ]
            });
        }
    })
    .catch(error => {
        console.log(error);
        let details = fetchToken;
        details.value = {balance: 0};
        this.setState({
            tokens: [
                ...this.state.tokens,
                details
            ]
        });
    });
  }

  fetchTokenOperations = (token) => {
    this.setState({
        fetchOps: token.token_symbol || token.contractId.substring(token.contractId.length-3, token.contractId.length),
        transactions: [],
        displayMore: false,
        fetching: true,
        counter: 1,
        contractHash: token.contract_hash || token.contractId,

    });
    axios.get(`${this.state.expFabrick}contract/${token.contract_hash || token.contractId}/ops/${this.state.userData.value[this.state.indexer]}?limit=10&order=desc`).then(response => {
        let ops = [];
        response.data.contract_ops.forEach(op => {
            if(op.parameters){
                op.entrypoint = op.parameters.entrypoint;
                op.parameters = op.parameters.value[op.parameters.entrypoint] || op.parameters.value;
                if(op.parameters.amount && token.token_factor)
                    op.parameters.amount = parseFloat(op.parameters.amount / token.token_factor);
                else if(op.parameters.value && token.token_factor)
                    op.parameters.value = parseFloat(op.parameters.value / token.token_factor);
            }
            ops.push(op);
        });
        this.setState({
            transactions: ops,
            displayMore: ops.length === 10? true : false,
        });
    }).catch(error => {
        console.log(error);
    })
    .finally(() => {
        this.setState({ fetching: false });
    });
  }

  switchNet = (network) => {
    this.setState({network: network,
                       expFabrick: `${FABRICK_EXP}${network}/`, tezosNodes: [`https://${network}.smartpy.io/`]});
    setTimeout(
        function() {
            this.fetchAccountDetails();
        }
        .bind(this),
        500
    );
  }

  switchWallet = (index) => {
    selectWallet(index);
    this.setState({indexer: index});
    setTimeout(
        function() {
            this.fetchAccountDetails();
        }
        .bind(this),
        500
    );
  }

  removeWallet = () => {
    const jwt = getJWT();
    if(jwt){
        this._asyncRequest = axios.delete(`${FABRICK_EXP.replace('tezos/', '')}account/wallet/address/${this.state.userData.value[this.state.indexer]}`,
            {headers: {'X-AUTH-USER': jwt}}).then(
                resp => {
                    rmAuth(this.state.indexer);
                    if(this.state.userData.value.length > 1)
                        location.reload();
                    else
                        window.location = '/auth/home';
                }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not remove address'});
        });
    }else{
        rmAuth(this.state.indexer);
        if(this.state.userData.value.length > 1)
            location.reload();
        else
            window.location = '/auth/home';
    }
  }

  fetchMoreOps = () => {
    this.setState({fetching: true});
    if(this.state.contractHash){
        axios.get(`${this.state.expFabrick}contract/${this.state.contractHash}/ops/${this.state.userData.value[this.state.indexer]}?limit=10&order=desc${this.state.displayMore && `&cursor=${this.state.counter * 10}`}`).then(response => {
            let ops = [];
            response.data.contract_ops.forEach(op => {
                if(op.parameters)
                    op.parameters = op.parameters.value[op.parameters.entrypoint] || op.parameters.value;
                    if(op.parameters.amount && token.token_factor)
                        op.parameters.amount = parseFloat(op.parameters.amount / token.token_factor);
                    else if(op.parameters.value && token.token_factor)
                        op.parameters.value = parseFloat(op.parameters.value / token.token_factor);
                ops.push(op);
            });
            this.setState({
                transactions: ops.reverse(),
                displayMore: ops.length === 10? true : false,
                counter: this.state.counter + 1,
            });
        }).catch(error => {
            console.log(error);
        })
        .finally(() => {
            this.setState({ fetching: false });
        });
    }else{
        axios.get(`${this.state.expFabrick}account/${this.state.userData.value[this.state.indexer]}/ops?limit=10&offset=${this.state.counter*10}`).then(response => {
            let ops = this.state.transactions;
            response.data.operations.forEach(op => {
                if(op.parameters)
                    op.parameters = op.parameters.value[op.parameters.entrypoint] || op.parameters.value;
                ops.push(op);
            });
            this.setState({
                transactions: ops,
                counter: this.state.counter + 1,
                displayMore: response.data.operations.length === 10? true : false,
                fetching: false,
            });
        }).catch(error => {
            console.log(error);
            this.setState({fetching: false});
        });
    }
  }

  copyElement = (element) => {
    navigator.clipboard.writeText(element);
  }

  toggleDisplayTransfer = (donate=false) => {
    if(this.state.displayTransfer){
        const userData = loadAuth();
        this.setState({userData: userData});
    }
    this.setState({displayTransfer: !this.state.displayTransfer, donate: donate});
    window.scrollTo(0, 0);
  }

  toggleDisplayCall = (feature, token) => {
    if(this.state.displayCall){
        const userData = loadAuth();
        this.setState({userData: userData});
    }
    this.setState({displayCall: !this.state.displayCall, feature: feature, selectedToken: token});
    window.scrollTo(0, 0);
  }

  toggleDisplayReveal = () => {
    this.setState({displayReveal: !this.state.displayReveal});
    window.scrollTo(0, 0);
  }

  toggleDisplayDelegate = () => {
    this.setState({displayDelegate: !this.state.displayDelegate});
    window.scrollTo(0, 0);
  }

  renderSuccessMsg = (msg) => {
    this.setState({successMsg: msg});
  }

  render() {
    return (
      <div className="main-content">
          <AccountNavbar expFabrick={this.state.expFabrick} network={this.state.network} indexer={this.state.indexer}/>
          {!this.state.displayTransfer && !this.state.displayReveal && !this.state.displayCall && !this.state.displayDelegate ?
              <>
                  <Header
                    network={this.state.network}
                    tezEur={this.state.tezEur}
                    switchNetwork={this.switchNet}
                    indexer={this.state.indexer}
                    blockHead={this.state.blockHead}
                    cycle={this.state.cycle}
                    userData={this.state.userData}
                    copyElement={this.copyElement}
                    tokens={this.state.tokens}
                    switchWallet={this.switchWallet}
                    accountDetails={this.state.accountDetails}
                    displayTransfer={this.state.displayTransfer}
                    displayReveal={this.state.displayReveal}
                    displayDelegate={this.state.displayDelegate}
                    successMsg={this.state.successMsg}
                    renderSuccessMsg={this.renderSuccessMsg}
                    toggleDisplayTransfer={this.toggleDisplayTransfer}
                    toggleDisplayReveal={this.toggleDisplayReveal}
                    toggleDisplayDelegate={this.toggleDisplayDelegate}
                    networks={this.state.networks}
                    removeWallet={this.removeWallet}
                    tezosNodes={this.state.tezosNodes}
                    totalBalance={this.state.totalBalance}
                    toggleDisplayCall={this.toggleDisplayCall}
                    maxWallets={this.state.maxWallets}/>
                  <Container className="mt--6" fluid>
                      <Operations userData={this.state.userData}
                                  indexer={this.state.indexer}
                                  copyElement={this.copyElement}
                                  fetchAccountDetails={this.fetchAccountDetails}
                                  transactions={this.state.transactions}
                                  fetchMoreOps={this.fetchMoreOps}
                                  displayMore={this.state.displayMore}
                                  fetching={this.state.fetching}
                                  tokens={this.state.tokens}
                                  fetchAccountDetails={this.fetchAccountDetails}
                                  fetchTokenOperations={this.fetchTokenOperations}
                                  fetchOps={this.state.fetchOps}
                                  expAPI={this.state.expAPI}
                                  expFabrick={this.state.expFabrick}/>
                  </Container>
              </>
          :
              <>
                  {this.state.displayTransfer && <Transfer
                    displayTransfer={this.state.displayTransfer}
                    toggleDisplayTransfer={this.toggleDisplayTransfer}
                    indexer={this.state.indexer}
                    userData={this.state.userData}
                    tezosNodes={this.state.tezosNodes}
                    donateAddress={this.state.donateAddress}
                    donate={this.state.donate}
                    renderSuccessMsg={this.renderSuccessMsg}
                    balance={this.state.accountDetails && this.state.accountDetails.spendable_balance || 0}/>}
                  {this.state.displayCall && <Call
                    displayCall={this.state.displayCall}
                    toggleDisplayCall={this.toggleDisplayCall}
                    indexer={this.state.indexer}
                    userData={this.state.userData}
                    tezosNodes={this.state.tezosNodes}
                    selectedToken={this.state.selectedToken}
                    feature={this.state.feature}
                    renderSuccessMsg={this.renderSuccessMsg}
                    balance={this.state.accountDetails && this.state.accountDetails.spendable_balance || 0}/>}
                  {this.state.displayReveal && <Reveal
                    displayReveal={this.state.displayReveal}
                    toggleDisplayReveal={this.toggleDisplayReveal}
                    indexer={this.state.indexer}
                    userData={this.state.userData}
                    tezosNodes={this.state.tezosNodes}
                    renderSuccessMsg={this.renderSuccessMsg}
                    balance={this.state.accountDetails && this.state.accountDetails.spendable_balance || 0}/>}
                {this.state.displayDelegate && <Delegate
                    displayDelegate={this.state.displayDelegate}
                    toggleDisplayDelegate={this.toggleDisplayDelegate}
                    indexer={this.state.indexer}
                    userData={this.state.userData}
                    tezosNodes={this.state.tezosNodes}
                    renderSuccessMsg={this.renderSuccessMsg}
                    balance={this.state.accountDetails && this.state.accountDetails.spendable_balance || 0}/>}
              </>
          }
        <AccountFooter />
      </div>
    );
  }
}

export default Dashboard;
