import React from "react";
import axios from "axios";
import Link from "next/link";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import Auth from "layouts/Auth.js";
import Router from "next/router";
import {signin, storeAuth,loadAuth} from '../../utils/user.js';

const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");
const emailRegex = /\S+@\S+\.\S+/;

class Validate extends React.Component {
  static getInitialProps ({ query: { id, token } }) {
    return { userId: id,  token: token}
  }
  constructor(props) {
    super(props);
    this.state = {
      validated: null,
      expFabrick: 'https://explorer-test.fabrick.tech/api/v1/account/'
    };
  };

  componentDidMount = () => {
    this._asyncRequest = axios.get(`${this.state.expFabrick}email/validate/${this.props.userId}/${this.props.token}`).then(
        res => {
            if(res.data.status){
                this.setState({validated: true});
            }
        }
    ).catch(error => {
        console.log(error);
    });
  };

  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="shadow border-0 card-blur">
            {this.state.validated?
                <CardBody className="px-lg-5 py-lg-5 text-secondary">
                    <div className="text-center mb-4">
                        <h1 className="text-white"><big><i className="far fa-check-circle"></i></big></h1>
                        <h2 className="text-yellow">Email validated</h2>
                        <p>Your email is now validated.
                          <a
                            className="text-info ml-1 mr-1"
                            href="/auth/login"
                          >
                            <strong>Login</strong>
                          </a>now</p>
                      </div>
                </CardBody>
            :
                <CardBody className="px-lg-5 py-lg-5 text-secondary">
                    <div className="text-center mb-4">
                        <h1 className="text-white"><big><i className="far fa-hourglass"></i></big></h1>
                        <h2 className="text-yellow">Pending validation...</h2>
                        <p>Please wait...</p>
                      </div>
                </CardBody>
            }
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="/auth/login"
                onClick={(e) => e.preventDefault()}
              >
                <small>Login</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="/auth/register"
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

Validate.layout = Auth;

export default Validate;
