import React from "react";
import axios from "axios";
import Link from "next/link";

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})");
const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");
const weakRegex = new RegExp("^(?=.{4,})");

const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: null,
      email: null,
      username: null,
      expFabrick: 'https://explorer-test.fabrick.tech/api/v1/account/register',
      error: null,
      success: false,
      pending: false
    };
  };

  updatePswd = (e) => {
    this.setState({password: e.target.value});
  }

  updateEmail = (e) => {
    this.setState({email: e.target.value});
  }

  updateUsername = (e) => {
    this.setState({username: e.target.value});
  }

  registerAccount = () => {
    this.setState({error: null, success: false});
    if(!this.state.email || !emailRegex.test(this.state.email)){
        this.setState({error: 'Invalid email address'});
    }else if(!this.state.password || !mediumRegex.test(this.state.password)){
        this.setState({error: 'Invalid password'});
    }else if(!this.state.username || this.state.username.length < 3){
        this.setState({error: 'Invalid Username'});
    }else{
        this.setState({pending: true});
        this._asyncRequest = axios.post(this.state.expFabrick, {
                                username: this.state.username,
                                email_address: this.state.email,
                                password: this.state.password
                            }
                        ).then(
            res => {
                //console.log(JSON.stringify(res));
                if(res.data){
                    this.setState({success: true, pending: false});
                }
            }
        ).catch(error => {
            console.log(res.error);
            this.setState({error: 'Could not register your account', pending: false});
        });
    }
  }

  render() {
    return (
      <>
        <Col lg="6" md="8">
          <Card className="shadow border-0 card-blur">
            <CardBody className="px-lg-5 py-lg-5 text-secondary">
              <div className="text-center mb-4">
                <h1 className="text-secondary"><big><i className="fa fa-user-circle"></i></big></h1>
                <h2 className="text-yellow">Register an account</h2>
                <p>Set your credentials</p>
              </div>
              {this.state.error && <p className="text-danger">{this.state.error}</p>}
              {this.state.success && <p className="text-success">
                Account successfully created. A validation token was sent on <b>{this.state.email}</b>
              </p>}
              <Form role="form">
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <Input placeholder="Username" type="text"
                      onChange={this.updateUsername}/>
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <Input
                      placeholder="Email"
                      type="email"
                      autoComplete="new-email"
                      onChange={this.updateEmail}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <Input
                      placeholder="Password"
                      type="password"
                      autoComplete="new-password"
                      onChange={this.updatePswd}
                    />
                  </InputGroup>
                </FormGroup>
                {this.state.password && <div className="text-center">
                    {strongRegex.test(this.state.password)?
                        <Badge color={'success'} className="text-white">
                          Strong
                          <small className="d-block">password</small>
                        </Badge>
                    :
                        mediumRegex.test(this.state.password)?
                            <Badge color={'warning'} className="text-white">
                              Medium
                              <small className="d-block">password</small>
                            </Badge>
                        :
                             weakRegex.test(this.state.password) &&
                                 <Badge color={'danger'} className="text-white">
                                    Weak
                                    <small className="d-block">password</small>
                                 </Badge>
                    }
                </div>}
                <Row className="my-4">
                  <Col xs="12">
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id="customCheckRegister"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customCheckRegister"
                      >
                        <span className="text-secondary">
                          I agree with the{" "}
                          <a href="#pablo" className="text-info" onClick={(e) => e.preventDefault()}>
                            Privacy Policy
                          </a>
                        </span>
                      </label>
                    </div>
                  </Col>
                </Row>
                <div className="text-center">
                {this.state.pending?
                    <span className="h2 font-weight-bold mb-0 text-info ml-4 mb-4">
                        <i className="fas fa-circle-notch fa-spin"></i>
                    </span>
                :
                  <>
                      {this.state.success?
                          <Link href="/auth/login"><Button className="mt-4 float-left" color="success" type="button">
                            Sign in
                          </Button></Link>
                      :
                          <Button className="mt-4 float-left" color="primary" type="button" onClick={() => this.registerAccount()}>
                            Register
                          </Button>
                      }
                      <Link href="/auth/home"><Button className="mt-4 float-right" color="secondary" type="button">
                        Cancel
                      </Button></Link>
                      <span className="text-secondary text-sm d-block mt-2 mb-0">
                          Already have an account?
                          <a href="/auth/login" className="text-info ml-1">
                                Sign in
                          </a>
                      </span>
                  </>
                }
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

Register.layout = Auth;

export default Register;
