import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  Col,
  Input,
  Row,
} from "reactstrap";
// layout for this page
import * as tezos from 'conseiljs';

const derivationPath = "44'/1729'/0'/0'/0'";

class Reveal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pending: false,
      error: null,
      ledgerKeystore: null,
      password: null,
    };
  };

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  unlockLedger = async () => {
        this.setState({loading: true, error: null});
        try{
            const keystore = await tezos.TezosLedgerWallet.unlockAddress(0, derivationPath);
            this.setState({loading: false, ledgerKeystore: keystore, error: null});
        }catch(error){
            this.setState({loading: false, error: 'Could not connect Ledger device'});
        }
  }

  ledgerSendOp= () => {
        if(!this.state.ledgerKeystore)
            this.setState({error: 'Please connect your ledger first'});
        else{
            this.setState({pending: true, error: null});
            this._asyncRequest = tezos.TezosNodeWriter.sendKeyRevealOperation(this.props.tezosNodes[0], this.state.ledgerKeystore).then(result => {
                this.setState({pending: false});
                this.props.renderSuccessMsg(`Reveal operation sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.props.userData.value[this.props.indexer]}</strong>`);
                this.props.toggleDisplayReveal()
            })
            .catch(error => {
                this.setState({error: error, pending: false});
            });
        }
    }

  keystoreSendOp= () => {
        if(!this.props.userData.type[this.props.indexer] === 'file')
            this.setState({error: 'Please select a Keystore wallet file'});
        else if(!this.state.password)
            this.setState({error: 'Missing parameters'});
        else{
            this.setState({pending: true, error: null});
            this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(this.props.userData.keystore[this.props.indexer]), this.state.password).then(loadedWallet => {
                console.log(this.props.tezosNodes[0]);
                this._asyncRequest = tezos.TezosNodeWriter.sendKeyRevealOperation(this.props.tezosNodes[0], loadedWallet.identities[0]).then(result => {
                    this.setState({pending: false});
                    this.props.renderSuccessMsg(`Reveal operation sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.props.userData.value[this.props.indexer]}</strong>`);
                    this.props.toggleDisplayReveal()
                })
                .catch(error => {
                    this.setState({error: error.toString(), pending: false});
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({error: 'Invalid password', pending: false});
            });
        }
    }

  render() {
    return (
        <Row className="card-operation">
            <Col md="3" className="d-none d-md-block"></Col>
            <Col xs="12" md="6">
                <Card className="card-stats card-blur mt-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h2"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Reveal account
                          </CardTitle>
                          <span className="text-white d-block text-sm">Available funds :
                              <span className="font-weight-bold mb-0 text-yellow shadow-sm ml-2 text-md">
                                {this.props.balance} ꜩ
                              </span>
                          </span>
                          <hr/>
                          {this.props.userData && this.props.userData.value[this.props.indexer] &&
                            <span className="text-white d-block text-sm">Reveal public key for <span className="text-yellow">{this.props.userData.value[this.props.indexer]}</span></span>
                          }
                          <span className="text-white d-block text-sm">Reveal fees of <span className="text-yellow">0.00127 ꜩ</span></span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-primary rounded-circle shadow">
                            <i className="fas fa-exchange-alt"></i>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                      {this.state.error && <span className="text-danger d-block ml-2 mt-2">
                           <small>{this.state.error}</small>
                        </span>}
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' &&
                            <Col md="12">
                                <p className="mt-3 mb-1 text-secondary text-sm">Password</p>
                                <Input type="password" placeholder="Password to unlock keystore file" className="ml-1 border-0"
                                onChange={(e) => this.updatePassword(e)}/>
                            </Col>
                        }
                        {this.state.ledgerKeystore && <span className="text-yellow d-block ml-2 mt-2">
                           <small>{this.state.ledgerKeystore.publicKeyHash} connected</small>
                        </span>}
                      </Row>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'ledger' && !this.state.ledgerKeystore && !this.state.loading &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2"
                                              onClick={this.unlockLedger}>
                                        Connect Ledger
                            </Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.loading &&
                            <small className="text-white">Follow instructions on your Ledger...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.ledgerKeystore && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.ledgerSendOp()}}>Reveal</Button>
                        }
                        {this.state.pending &&
                            <small className="text-secondary">Sending reveal operation...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'file' && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.keystoreSendOp()}}>Reveal</Button>
                        }

                        <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right"  onClick={() => {this.props.toggleDisplayReveal()}}>Cancel</Button>
                    </CardFooter>
                  </Card>
            </Col>
            <Col md="3" className="d-none d-md-block"></Col>
        </Row>
    );
  }
}

export default Reveal;
