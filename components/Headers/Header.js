import React from "react";
import Link from "next/link";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Container,
  Row,
} from "reactstrap";

import {shrinkString, truncStringPortion} from '../../utils/helper.js';
import {loadContracts} from '../../utils/user.js';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        copied: false,
        dropdownOpen: false,
        dropdownNetwork: false,
        moreDetails: null,
        loadedContracts: [],
    };
  };

  toggleDropdown = () => {
    this.setState({dropdownOpen: !this.state.dropdownOpen});
  }

  toggleNetwork = () => {
    this.setState({dropdownNetwork: !this.state.dropdownNetwork});
  }

  processCopy = () =>{
    this.props.copyElement(this.props.userData.value[this.props.indexer]);
    this.setState({copied: true});
    setTimeout(
        function() {
            this.setState({copied: false});
        }
        .bind(this),
        2000
    );
  }

  componentDidMount = () => {
    const loadedContracts = loadContracts();
    console.log(loadedContracts);
    this.setState({loadedContracts: loadedContracts});
  }

  render() {
    return (
      <>
        <div className="header pb-6 pb-md-8 pt-2 pt-md-5">
          <Container fluid>
            <div className="header-body">
              <Row>

                {this.props.successMsg &&
                    <Col xs="12">
                        <div className="bg-yellow p-2 rounded text-sm d-block mb-2">
                            <span className="cursor-pointer float-right mr-2" onClick={() => {this.props.renderSuccessMsg(null)}}>
                                <i className="far fa-times-circle"></i>
                            </span>
                            <span dangerouslySetInnerHTML={{__html: this.props.successMsg}}>
                            </span>
                            <small className="d-block">It might take take up to 1 min. to be validated and appear on the chain.</small>
                        </div>
                    </Col>
                }
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Total spendable Balance
                          </CardTitle>
                          {this.props.accountDetails?
                              <span className="h2 font-weight-bold mb-0 text-yellow shadow-sm">
                                {this.props.totalBalance}
                              </span>
                          :
                              <span className="h2 font-weight-bold mb-0 text-info ml-4">
                                <i className="fas fa-circle-notch fa-spin"></i>
                              </span>
                          }
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-yellow text-dark rounded-circle shadow">
                            <strong>ꜩ</strong>
                          </div>
                        </Col>
                      </Row>
                      {this.props.accountDetails && <p className="mt-3 mb-0 text-secondary text-sm">
                        <strong className="text-yellow mr-2">
                            {Math.round(parseFloat(this.props.tezEur * this.props.totalBalance)*100) / 100} €
                        </strong>
                      </p>}
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        <small className="d-block text-white mt-2 mb-2">{this.props.userData && this.props.userData.value.length} {this.props.userData && this.props.userData.value.length > 1? `wallets`: `wallet`} referenced | Max. {this.props.maxWallets} wallets</small>
                    </CardFooter>
                  </Card>
                </Col>
                <Col xs="12" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur" style={{zIndex: "5"}}>
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Selected wallet
                          </CardTitle>
                            {this.props.userData &&
                                <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown} size="sm">
                                  <DropdownToggle caret color="primary">
                                     {this.props.userData.alias && this.props.userData.alias[this.props.indexer]?
                                        `${this.props.userData.alias[this.props.indexer]} (${truncStringPortion(this.props.userData.value[this.props.indexer], 6, 3)})`
                                     :
                                        this.props.userData.value[this.props.indexer]
                                     }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                        {this.props.userData && this.props.userData.value.map((key, index) =>
                                            <DropdownItem key={key} onClick={() => {this.props.switchWallet(index)}}>
                                                {this.props.userData.type[index] === 'ledger'?
                                                    <i className="fab fa-usb text-info align-middle mr-2"></i>
                                                :
                                                    this.props.userData.type[index] === 'file'?
                                                        <i className="fas fa-file text-info align-middle mr-2"></i>
                                                    :
                                                        <i className="fas fa-wallet text-info align-middle mr-2"></i>
                                                }
                                                <span>
                                                    {this.props.userData.alias && this.props.userData.alias[index]?
                                                        `${this.props.userData.alias[index]} (${truncStringPortion(key, 6, 3)})`
                                                    :
                                                        key
                                                    }
                                                </span>

                                            </DropdownItem>
                                        )}
                                  </DropdownMenu>
                                </Dropdown>
                            }
                            {this.state.copied?
                                <small className="ml-2 text-success">copied!</small>
                            :
                                <i className="fas fa-copy cursor-pointer ml-2 text-primary"
                                    onClick={() => {this.processCopy()}}></i>
                            }
                        </div>
                        <Col className="col-auto d-none d-md-block">
                          <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                            <i className="fas fa-wallet" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-secondary text-sm">
                        <span className="text-nowrap">INFO <i className="fas fa-chevron-right"></i>
                            {this.props.userData && <strong className="ml-2 mr-2">
                                {this.props.userData.type[this.props.indexer] === 'address'?
                                    'Public address | Read-only | '
                                :
                                    this.props.userData.type[this.props.indexer] === 'file'?
                                        'Keystore file | All operations allowed'
                                    :
                                        'Ledger Nano | All operations allowed'
                                }
                            </strong>}
                            {this.props.accountDetails && !this.props.accountDetails.is_revealed?
                                this.props.userData && this.props.userData.type[this.props.indexer] !== 'address' &&
                                <small className="text-nowrap cursor-pointer bg-primary p-2 rounded ml-2 text-dark" onClick={() => {this.props.toggleDisplayReveal()}}>Click to reveal</small>
                            :
                                <small className="text-white ml-2">
                                  <i className="fa fa-eye" /> Revealed
                                </small>
                            }
                        </span>
                      </p>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        <Button size="sm" color="primary" className="bg-gradient-success border-0 p-2 ml-1 mr-1" onClick={() => {this.props.switchWallet(this.props.indexer)}}><i className="fas fa-sync-alt"></i> Refresh</Button>
                        <Link href="/auth/home"><Button size="sm" color="primary" className="bg-primary border-0 p-2 ml-1 mr-1">Add a wallet</Button></Link>
                        <Link href="/auth/createwallet"><Button size="sm" color="primary" className="bg-primary border-0 p-2 ml-1 mr-1">Create new wallet</Button></Link>
                        <Button size="sm" color="danger" className="bg-danger border-0 mt-1 float-right d-block ml-1 mr-1"  onClick={() => {if(window.confirm('Remove this address?')){this.props.removeWallet()};}}>Remove</Button>
                    </CardFooter>
                  </Card>
                </Col>
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Network
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0 text-white">
                            {this.props.network &&
                                this.props.network.charAt(0).toUpperCase() + this.props.network.slice(1)}
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                            <i className="fas fa-project-diagram" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-secondary text-sm">
                        Block Id <strong className="text-yellow">#{this.props.blockHead.toLocaleString()}</strong> | Cycle <strong className="text-yellow">{this.props.cycle}</strong>
                      </p>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white" style={{zIndex: "4"}}>
                        <small>
                            <span className="text-nowrap mb-2">Switch to </span>
                            <Dropdown isOpen={this.state.dropdownNetwork} toggle={this.toggleNetwork} size="sm">
                              <DropdownToggle caret color="success">
                                 {this.props.network.charAt(0).toUpperCase() + this.props.network.slice(1)}
                              </DropdownToggle>
                              <DropdownMenu>
                                    {this.props.networks && this.props.networks.map(network =>
                                        <DropdownItem key={network} onClick={() => {this.props.switchNetwork(network)}}>
                                            <span>{network.charAt(0).toUpperCase() + network.slice(1)}</span>
                                        </DropdownItem>
                                    )}
                              </DropdownMenu>
                            </Dropdown>
                        </small>
                    </CardFooter>
                  </Card>
                </Col>
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 mt-4 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            XTZ
                          </CardTitle>
                          {this.props.accountDetails?
                              <span className="h2 font-weight-bold mb-0 text-yellow shadow-sm">
                                {this.props.accountDetails.spendable_balance}
                              </span>
                          :
                              <span className="h2 font-weight-bold mb-0 text-info ml-4">
                                <i className="fas fa-circle-notch fa-spin"></i>
                              </span>
                          }
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                            <strong>ꜩ</strong>
                          </div>
                        </Col>
                      </Row>
                      {this.props.accountDetails && <p className="mt-3 mb-0 text-secondary text-sm">
                        <strong className="text-yellow mr-2">
                            {Math.round(parseFloat(this.props.tezEur * this.props.accountDetails.spendable_balance)*100) / 100} €
                        </strong>
                      </p>}
                      {this.props.accountDetails && this.props.accountDetails.delegate && <small className="text-white">Funds delegated to {truncStringPortion(this.props.accountDetails.delegate, 6, 3)}</small>}
                    </CardBody>
                    {this.props.userData && this.props.userData.type[this.props.indexer] !== 'address' && this.props.accountDetails && <CardFooter className="bg-transparent text-white">
                        {this.props.accountDetails.is_revealed?
                          <>
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2 ml-1 mr-1" onClick={() => {this.props.toggleDisplayTransfer()}}>Transfer ꜩ</Button>
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2 ml-1 mr-1" onClick={() => {this.props.toggleDisplayDelegate()}}>Delegate</Button>
                            <Button size="sm" color="primary" className="bg-gradient-success border-0 p-2 ml-1 mr-1" onClick={() => {this.props.toggleDisplayTransfer(true)}}>Donate</Button>
                          </>
                        :
                          <small className="text-nowrap cursor-pointer text-primary" onClick={() => {this.props.toggleDisplayReveal()}}>Reveal to transfer</small>
                        }
                    </CardFooter>}
                  </Card>
                </Col>
                {this.props.tokens && this.props.tokens.map((token) => (
                    <Col xs="12" lg="6" xl="3" key={token.token_symbol}>
                      <Card className="card-stats mb-4 mb-xl-0 mt-4 card-blur">
                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle
                                tag="h5"
                                className="text-uppercase text-secondary mb-0"
                              >
                                {token.token_name}
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-yellow">
                                {token.value.balance && token.value.balance > 0? token.value.balance : 0}
                              </span>
                            </div>
                            <Col className="col-auto">
                              {token.about && token.about.logo?
                                  <div className="icon icon-shape" 
                                      style={{backgroundImage: `url(${token.about.logo})`, backgroundPosition: "center center", backgroundSize: "cover"}}
                                      onClick={() => {window.open(token.about.website)}}
                                  >
                                  </div>
                                  :
                                  <div className="icon icon-shape bg-primary text-white rounded-circle shadow">
                                    {token.token_symbol}
                                  </div>
                              }
                            </Col>
                          </Row>
                          <p className="mt-2 mb-0 text-secondary text-sm">
                            {this.state.moreDetails === token.token_symbol?
                                 <span className="mw-100">
                                    <small>{token.about.description} <br/> <a href={token.about.website} target="_blank">{token.about.website}</a></small>
                                    <small className="text-yellow cursor-pointer d-block" onClick={() => {this.setState({moreDetails: null})}}>- Reduce</small>
                                 </span>
                            :
                                <span>
                                    <small>{token.about && shrinkString(token.about.description, 38, 3)}</small>
                                    <small className="text-yellow cursor-pointer ml-2" onClick={() => {this.setState({moreDetails: token.token_symbol})}}>+ More</small>
                                 </span>
                            }
                          </p>
                        </CardBody>
                        {this.props.userData && this.props.userData.type[this.props.indexer] !== 'address' && this.props.accountDetails &&
                            <CardFooter className="bg-transparent text-white">
                                {this.props.accountDetails.is_revealed?
                                  <>
                                    {token.entry_points && token.entry_points.map(feature =>
                                        <Button size="sm" color="primary" className="bg-primary border-0 p-2" key={feature.display} onClick={() => {this.props.toggleDisplayCall(feature, token)}}>{feature.display}</Button>
                                    )}
                                  </>
                                :
                                  <small className="text-nowrap cursor-pointer text-primary" onClick={() => {this.props.toggleDisplayReveal()}}>Reveal to send operations</small>
                                }
                            </CardFooter>
                        }
                      </Card>
                    </Col>
                ))}
                {this.state.loadedContracts.map((contract) => ( contract.network === this.props.network &&
                    <Col xs="12" lg="6" xl="3" key={contract.contractId}>
                      <Card className="card-stats mb-4 mb-xl-0 mt-4 card-blur">
                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle
                                tag="h5"
                                className="text-uppercase text-secondary mb-0"
                              >
                                Custom contract
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-yellow">
                                {truncStringPortion(contract.contractId, 6, 3)}
                              </span>
                            </div>
                            <Col className="col-auto">
                              <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                                {contract.contractId.substring(contract.contractId.length-3, contract.contractId.length)}
                              </div>
                            </Col>
                          </Row>
                          <p className="mt-2 mb-0 text-secondary text-sm">
                            <span>
                                <small>No description available</small>
                            </span>
                          </p>
                        </CardBody>
                        {this.props.userData && this.props.userData.type[this.props.indexer] !== 'address' && this.props.accountDetails &&
                            <CardFooter className="bg-transparent text-white">
                                <Button size="sm" color="primary" className="bg-primary border-0 p-2"  onClick={() => {location.href=`/account/contract?network=${this.props.network}&indexer=${this.props.indexer}&contract=${contract.contractId}`}}>Call feature</Button>
                            </CardFooter>
                        }
                      </Card>
                    </Col>
                ))}
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 mt-4 bg-gradient-blue border-0">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Set custom contract
                          </CardTitle>
                          <Button size="sm" color="primary" className="bg-primary border-0 p-2 mt-3" onClick={() => {location.href=`/account/contract?network=${this.props.network}&indexer=${this.props.indexer}`}}>
                              Set KT1 address
                          </Button>
                        </div>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
      </>
    );
  }
}

export default Header;
