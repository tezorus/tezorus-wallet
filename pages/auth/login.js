import React from "react";
import axios from "axios";
import Link from "next/link";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";
import Router from "next/router";
import {signin, storeAuth,loadAuth} from '../../utils/user.js';

const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: null,
      username: null,
      expFabrick: 'https://explorer-test.fabrick.tech/api/v1/account/',
      error: null,
      success: false,
      pending: false,
      jwtoken: null
    };
  };

  updatePswd = (e) => {
    this.setState({password: e.target.value});
  }

  updateUsername = (e) => {
    this.setState({username: e.target.value});
  }

  loginAccount = () => {
    this.setState({error: null, success: false});
    if(!this.state.password || !mediumRegex.test(this.state.password)){
        this.setState({error: 'Invalid password'});
    }else if(!this.state.username || this.state.username.length < 3){
        this.setState({error: 'Invalid Username or Email'});
    }else{
        this.setState({pending: true});
        this._asyncRequest = axios.post(`${this.state.expFabrick}login`, {
                                login: this.state.username,
                                password: this.state.password
                            }
                        ).then(
            res => {
                if(res.data){
                    const jwtoken = res.data.jwt_token;
                    signin(jwtoken);
                    const userData = loadAuth();
                    if(userData){
                        userData.value.forEach((wallet, index) => {
                            this._asyncRequest = axios.post(`${this.state.expFabrick}wallet/address`, {
                                    address: userData.value[index],
                                    wallet_type: userData.type[index],
                                    blockchain: "tezos"
                                },
                                {headers: {'X-AUTH-USER': jwtoken}}).then(
                                    resp => {
                                        console.log(resp.data.message);
                                    }
                            ).catch(error => {
                                console.log(res.error);
                            });
                        });
                    }
                    this._asyncRequest = axios.get(`${this.state.expFabrick}wallet/address`,
                        {headers: {'X-AUTH-USER': jwtoken}}).then(
                            resp => {
                                if(resp.data){
                                    this.setState({success: true, pending: false});
                                    resp.data.wallets.forEach(wallet => {
                                        const keystoreAuth = {
                                            type: [wallet.wallet_type],
                                            value: [wallet.wallet_address],
                                            keystore: [{}],
                                            alias: [wallet.alias],
                                        }
                                        storeAuth(keystoreAuth);
                                    });
                                    Router.push('/account/dashboard');
                                }
                            }
                    ).catch(error => {
                        console.log(error);
                        this.setState({error: 'Could not load addresses', pending: false});
                    });
                }
            }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not login', pending: false});
        });
    }
  }

  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="shadow border-0 card-blur">
            <CardBody className="px-lg-5 py-lg-5 text-secondary">
              <div className="text-center mb-4">
                <h1 className="text-white"><big><i className="fa fa-sign-in-alt"></i></big></h1>
                <h2 className="text-yellow">Sign in</h2>
                <p>Set your credentials to access your personal Dashboard</p>
              </div>
              {this.state.error && <small className="d-block bg-danger font-size-10 rounded text-white mt-2 mb-2 p-1 pl-3">{this.state.error}</small>}
              {this.state.success && <small className="d-block bg-success font-size-10 rounded text-white mt-2 mb-2 p-1 pl-3">Successfully signed</small>}
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <Input
                      placeholder="Username or email"
                      type="text"
                      autoComplete="new-email"
                      onChange={this.updateUsername}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <Input
                      placeholder="Password"
                      type="password"
                      autoComplete="new-password"
                      onChange={this.updatePswd}
                      onKeyPress={event => {
                        if (event.key === 'Enter') {
                          this.loginAccount()
                        }
                      }}
                    />
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  {this.state.pending?
                        <span className="h2 font-weight-bold mb-0 text-info ml-4 mb-4">
                            <i className="fas fa-circle-notch fa-spin"></i>
                        </span>
                  :
                      <>
                          <Button className="mt-4 float-left" color="primary" type="button" onClick={() => this.loginAccount()}>
                            Sign in
                          </Button>
                          <Link href="/auth/home"><Button className="mt-4 float-right" color="secondary" type="button">
                            Cancel
                          </Button></Link>
                      </>
                  }
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="/auth/forgottenpassword"
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="/auth/register"
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

Login.layout = Auth;

export default Login;
