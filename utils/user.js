import Router from "next/router";
import axios from "axios";

export function storeAuth(keystore) {
    if (keystore) {
        if(sessionStorage.getItem('keysStore')){
            let userStorage = JSON.parse(sessionStorage.getItem('keysStore'));
            let match = false;
            userStorage.value.forEach((wallet, index) => {
                if(wallet === keystore.value[0]){
                    match = true;
                    userStorage.type[index] = keystore.type[0];
                    userStorage.keystore[index] = keystore.keystore[0];
                    userStorage.alias[index] = keystore.alias[0];
                }
            })
            if(!match){
                userStorage.type.push(keystore.type[0]);
                userStorage.value.push(keystore.value[0]);
                userStorage.keystore.push(keystore.keystore[0]);
                userStorage.alias.push(keystore.alias[0]);
            }
            sessionStorage.setItem('keysStore', JSON.stringify(userStorage));
        }else{
            keystore.network = 'mainnet';
            keystore.indexer = 0;
            sessionStorage.setItem('keysStore', JSON.stringify(keystore));
        }
        return true;
    } else {
        return false;
    }
}

export function rmAuth(indexer) {
    let userStorage = JSON.parse(sessionStorage.getItem('keysStore'));
    userStorage.type.splice(indexer, 1);
    userStorage.value.splice(indexer, 1);
    userStorage.keystore.splice(indexer, 1);
    userStorage.alias.splice(indexer, 1);
    sessionStorage.setItem('keysStore', JSON.stringify(userStorage));
    return true;
}

export function loadAuth() {
    if(sessionStorage.getItem('keysStore'))
        return JSON.parse(sessionStorage.getItem('keysStore'));
    else
        return null;
}

export function logOut() {
    sessionStorage.removeItem('keysStore');
    sessionStorage.removeItem('fabrickJWT');
    location.reload();
}

export function switchNetwork(network) {
    let userStorage = JSON.parse(sessionStorage.getItem('keysStore'));
    userStorage.network = network;
    sessionStorage.setItem('user', JSON.stringify(userStorage));
}

export function selectWallet(indexer) {
    let userStorage = JSON.parse(sessionStorage.getItem('keysStore'));
    userStorage.indexer = indexer;
    sessionStorage.setItem('user', JSON.stringify(userStorage));
}

export function signin(jwt) {
    sessionStorage.setItem('fabrickJWT', jwt);
}

export function getJWT() {
    return sessionStorage.getItem('fabrickJWT');
}

export function saveContract(contractId, network) {
    console.log(`SAVING CONTRACT ${contractId}`);
    if(sessionStorage.getItem('contract')){
        let contracts = JSON.parse(sessionStorage.getItem('contract'));
        contracts.push({contractId: contractId, network: network})
    }else{
        sessionStorage.setItem('contract', JSON.stringify([{contractId: contractId, network: network}]));
    }
}

export function loadContracts() {
    console.log(`LOADING CONTRACTS ${sessionStorage.getItem('contract')}`);
    if(sessionStorage.getItem('contract')){
        return JSON.parse(sessionStorage.getItem('contract'));
    }else{
        return [];
    }
}
