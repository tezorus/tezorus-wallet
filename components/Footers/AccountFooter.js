/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Nav, NavItem, NavLink } from "reactstrap";

class AccountFooter extends React.Component {
  render() {
    return (
      <footer className="footer p-5 mt-3">
        <Row className="align-items-center justify-content-xl-between">
          <Col xl="6">
            <div className="copyright text-center text-xl-left text-muted">
              © {new Date().getFullYear()}{" "}
              <a
                className="font-weight-bold ml-1"
                href="https://www.tezorus.com"
                rel="noopener noreferrer"
                target="_blank"
              >
                tezOrus
              </a>
            </div>
          </Col>

          <Col xl="6">
            <Nav className="nav-footer justify-content-center justify-content-xl-end">
            <NavItem>
                    <NavLink
                      href="https://discord.gg/bcevXG8CvU"
                      target="_blank"
                    >
                      Discord
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://t.me/tezorus"
                      target="_blank"
                    >
                      Telegram
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://www.tezorus.com"
                      target="_blank"
                    >
                      About Us
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="https://gitlab.com/tezorus"
                      target="_blank"
                    >
                      Gitlab
                    </NavLink>
                  </NavItem>
            </Nav>
          </Col>
        </Row>
      </footer>
    );
  }
}

export default AccountFooter;
