import React from "react";
import axios from "axios";
// reactstrap components
import { Container } from "reactstrap";
// layout for this page
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import {switchNetwork} from '../../utils/user.js';

import AddressHeader from "components/Headers/AddressHeader.js";
import Operations from "./components/operations.js";

class Address extends React.Component {
  static getInitialProps ({ query: { address, network } }) {
    return { address: address, network: network }
  }

  constructor(props) {
    super(props);
    this.state = {
      address: null,
      network: "mainnet",
      expUrl: "https://api.tzstats.com/explorer/",
      accountDetails: null,
      tokens: [],
      expFabrick: 'https://explorer.fabrick.tech/api/v1/tezos/mainnet/',
      transactions: null,
      fetching: true,
      counter: 1,
      blockHead: 0,
      cycle: 0,
      fetchOps: 'xtz'
    };
  };

  componentDidMount = () => {
    this.setState({address: this.props.address,
                   network: this.props.network,
                   expUrl: this.props.network === 'mainnet'?
                           "https://api.tzstats.com/explorer/" : "https://api.delphi.tzstats.com/explorer/",
                   expFabrick: this.props.network === 'mainnet'?
                           "https://explorer.fabrick.tech/api/v1/tezos/mainnet/" :
                           "https://explorer.fabrick.tech/api/v1/tezos/delphinet/"});
    setTimeout(
        function() {
            this.fetchAccountDetails();
        }
        .bind(this),
        500
    );
  };

  fetchAccountDetails = () => {
    this.setState({accountDetails: null, tokens: [], fetching: true, transactions: [], fetchOps: 'xtz'});
    this._asyncRequest = axios.get(`${this.state.expUrl}account/${this.state.address}`).then(
        result => {
            if(result.data){
                this.setState({accountDetails: result.data});
                this._asyncRequest = axios.get(`${this.state.expUrl}account/${this.state.address}/operations?limit=10&order=desc`).then(
                    res => {
                        if(res.data){
                            this.setState({transactions: res.data,
                               displayMore: res.data.length === 10? true : false,
                               counter: 1,
                               fetching: false});
                        }
                    }
                ).catch(error => {
                    console.log(error);
                    this.setState({transactions: [], fetching: false});
                });
                this._asyncRequest = axios.get(`${this.state.expUrl}block/head`).then(
                    res => {
                        if(res.data){
                            this.setState({blockHead: res.data.height,
                               cycle: res.data.cycle});
                        }
                    }
                ).catch(error => {
                    console.log(error);
                    this.setState({transactions: [], fetching: false});
                });
            }
        }
    ).catch(error => {
        this.setState({accountDetails: {
                    spendable_balance: 0,
                    n_ops: 0,
                    is_revealed: false,
                    total_received: 0,
                    total_sent: 0,
                    total_burned: 0,
                    total_fees_paid: 0}, transactions: [], fetching: false});
    });
    this._asyncRequest = axios.get(`${this.state.expFabrick}tezorus/config`).then(response => {
        response.data.tokens.forEach(token => {
            this.fetchTokenDetails(token);
        });
    })
    .catch(error => {
        console.log(error);
    })
  }

  fetchTokenDetails = (fetchToken) => {
    axios.get(`${this.state.expUrl}bigmap/${fetchToken.bigmap_id}/${this.state.address}`).then(response => {
        let details = fetchToken;
        details.value = response.data.value;
        this.setState({
            tokens: [
              ...this.state.tokens,
              details
            ]
        });
    })
    .catch(error => {
        let details = fetchToken;
            details.value = {balance: 0};
            this.setState({
                tokens: [
                  ...this.state.tokens,
                  details
                ]
            });
    });
  }

  fetchTokenOperations = (token) => {
    this.setState({
        fetchOps: token.token_symbol,
        transactions: [],
        displayMore: false,
        fetching: true,
        counter: 1

    });
    console.log(`${this.state.expFabrick}contract/${token.contract_hash}/ops/${this.state.address}`);
    axios.get(`${this.state.expFabrick}contract/${token.contract_hash}/ops/${this.state.address}`).then(response => {
        this.setState({
            transactions: response.data.contract_ops.reverse()
        });
    }).catch(error => {
        console.log(error);
    })
    .finally(() => {
        this.setState({ fetching: false });
    });
  }

  switchNet = () => {
    if(this.state.network === "mainnet"){
        this.setState({network: "delphinet",
                       expUrl: "https://api.delphi.tzstats.com/explorer/",
                       expFabrick: "https://explorer.fabrick.tech/api/v1/tezos/delphinet/"});
        switchNetwork("delphinet");
    }else{
        this.setState({network: "mainnet",
                       expUrl: "https://api.tzstats.com/explorer/",
                       expFabrick: "https://explorer.fabrick.tech/api/v1/tezos/mainnet/"});
        switchNetwork("mainnet");
    }
    setTimeout(
        function() {
            this.fetchAccountDetails();
        }
        .bind(this),
        500
    );
  }

  copyElement = (element) => {
    navigator.clipboard.writeText(element);
  }

  fetchMoreOps = () => {
    // Fetch operations from a last fetched block
    this.setState({fetching: true, fetchOps: 'xtz'});
    this._asyncRequest = axios.get(`${this.state.expUrl}account/${this.state.userData.value[this.state.indexer]}/operations?limit=10&offset=${this.state.counter * 10}&order=desc`).then(
        res => {
            if(res.data){
                this.setState({
                  transactions: this.state.transactions.concat(res.data),
                  displayMore: res.data.length === 10? true : false,
                  fetching: false,
                  counter: this.state.counter + 1
                })
            }
        }
    ).catch(error => {
        console.log(error);
        this.setState({fetching: false});
    });
  }

  render() {
    return (
      <div className="main-content">
          <AccountNavbar expUrl={this.state.expUrl} />
          <AddressHeader
            network={this.state.network}
            switchNetwork={this.switchNet}
            blockHead={this.state.blockHead}
            cycle={this.state.cycle}
            address={this.state.address}
            copyElement={this.copyElement}
            tokens={this.state.tokens}
            accountDetails={this.state.accountDetails}/>
          <Container className="mt--6" fluid>
              <Operations userData={{value: [this.state.address]}}
                          indexer={0}
                          copyElement={this.copyElement}
                          fetchAccountDetails={this.fetchAccountDetails}
                          transactions={this.state.transactions}
                          fetchMoreOps={this.fetchMoreOps}
                          displayMore={this.state.displayMore}
                          fetching={this.state.fetching}
                          tokens={this.state.tokens}
                          fetchAccountDetails={this.fetchAccountDetails}
                          fetchTokenOperations={this.fetchTokenOperations}
                          fetchOps={this.state.fetchOps}
                          expUrl={this.state.expUrl}/>
          </Container>
          <Container fluid>
            <AccountFooter />
          </Container>
      </div>
    );
  }
}

export default Address;
