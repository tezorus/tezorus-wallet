import React from "react";
// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Table,
  Row,
} from "reactstrap";
// layout for this page
import {truncStringPortion} from '../../../utils/helper.js';
import {loadContracts} from '../../../utils/user.js';

class Operations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedContracts: [],
    };
  };

  componentDidMount = () => {
    const loadedContracts = loadContracts();
    console.log(loadedContracts);
    this.setState({loadedContracts: loadedContracts});
  }

  render() {
    return (
      <>
          <Row>
            <div className="col">
              <Card className="card-blur shadow">
                <CardHeader className="border-0 bg-transparent">
                  <h3 className="mb-2 text-white d-inline">Operations</h3>
                  <Button size="sm"
                          color={this.props.fetchOps === 'xtz' ? "primary" : "secondary"}
                          className="border-0 p-2 ml-2"
                          onClick={() => {this.props.fetchAccountDetails()}}>ꜩ</Button>
                  {this.props.tokens && this.props.tokens.map(token =>
                    <Button size="sm"
                            color={this.props.fetchOps === token.token_symbol ? "primary" : "secondary"}
                            className="border-0 p-2 ml-2"
                            key={token.token_symbol}
                            onClick={() => {this.props.fetchTokenOperations(token)}}>{token.token_symbol}</Button>
                  )}
                  {this.state.loadedContracts.map(contract =>
                    <Button size="sm"
                            color={this.props.fetchOps === contract.contractId.substring(contract.contractId.length-3, contract.contractId.length) ? "primary" : "secondary"}
                            className="border-0 p-2 ml-2"
                            key={contract.contractId}
                            onClick={() => {this.props.fetchTokenOperations(contract)}}>{contract.contractId.substring(contract.contractId.length-3, contract.contractId.length)}</Button>
                  )}
                </CardHeader>
                {this.props.transactions &&
                    <Table className="align-items-center table-flush text-secondary" responsive>
                      <thead className="thead-light">
                        <tr>
                          <th scope="col" className="bg-transparent">Op hash</th>
                          <th scope="col" className="bg-transparent">Type</th>
                          <th scope="col" className="bg-transparent">Date</th>
                          <th scope="col" className="bg-transparent">Amount</th>
                          <th scope="col" className="bg-transparent">Status</th>
                          <th scope="col" className="bg-transparent">Parameters</th>
                          <th scope="col" className="bg-transparent" />
                        </tr>
                      </thead>
                      <tbody>
                        {this.props.transactions && this.props.transactions.length > 0
                            && this.props.transactions.map((op, index) => (
                            <tr key={`${op.hash}-${index}`}>
                              <th scope="row">
                                {op.sender === this.props.userData.value[this.props.indexer] ?
                                    <span className="text-warning mr-2"><i className="fas fa-arrow-left"></i></span>
                                :
                                    <span className="text-success mr-2"><i className="fas fa-arrow-right"></i></span>
                                }
                                {truncStringPortion(op.hash, 7, 6)}
                              </th>
                              <td>
                                {op.parameters?
                                    <Badge color={'dark'} size="md">
                                      <span className="text-white">{op.entrypoint}</span>
                                      <small className="d-block text-white mt-2">Smart contract call</small>
                                    </Badge>
                                :
                                    <Badge color={'primary'}>
                                      <span className="text-white">{op.type === 'transaction'? 'transfer ꜩ' : op.type}</span>
                                    </Badge>
                                }
                              </td>
                              <td>
                                {new Date(op.time).toLocaleDateString(navigator.userLanguage, {hour: '2-digit', minute:'2-digit'})}
                              </td>
                              <td>
                                {this.props.fetchOps === 'xtz'?
                                    <h4 className="text-yellow">{op.volume} ꜩ</h4>
                                :
                                    typeof op.parameters !== 'string'?
                                        <h4 className="text-yellow">{op.parameters.value || op.parameters.amount} {this.props.fetchOps}</h4>
                                    :
                                        <h4 className="text-yellow">{op.volume} ꜩ</h4>
                                }
                                <small className="d-block">Baker fees: {op.fee} ꜩ</small>
                                {op.burned > 0 && <small className="d-block">Gas burnt: {op.burned} ꜩ</small>}
                              </td>
                              <td>
                                {op.status === 'applied'?
                                    <Badge color="" className="badge-dot text-success">
                                      <i className="bg-success"/>
                                      <small>{op.status}</small>
                                    </Badge>
                                :
                                    <Badge color="" className="badge-dot text-danger">
                                      <i className="bg-danger"/>
                                      <small>{op.status}</small>
                                    </Badge>
                                }
                              </td>
                              <td>
                                <span className="mb-0 d-block">
                                      <small>
                                        {op.parameters? 'caller:' : 'sender:'}
                                        <span className={op.sender === this.props.userData.value[this.props.indexer]? "text-yellow fw-semi-bold" : "text-secondary fw-semi-bold"}>
                                          &nbsp; {op.sender}
                                        </span>
                                      </small>
                                </span>
                                <span className="mb-0 d-block">
                                      <small>
                                        {op.parameters? 'contract:' : 'receiver:'}
                                        <span className={op.receiver === this.props.userData.value[this.props.indexer]? "text-yellow fw-semi-bold" : "text-secondary fw-semi-bold"}>
                                          &nbsp; {op.receiver}
                                        </span>
                                      </small>
                                </span>
                                {op.parameters && <div>
                                    {typeof op.parameters !== 'string'?
                                        Object.keys(op.parameters).map((e, i) => op.parameters[e] &&
                                            <span className="mb-0 d-block" key={e}>
                                                  <small>
                                                    {e}:
                                                    <span className={op.parameters[e].toString() === this.props.userData.value[this.props.indexer]? "text-yellow fw-semi-bold" : "text-secondary fw-semi-bold"}>
                                                      &nbsp; {op.parameters[e].toString().length > 36? truncStringPortion(op.parameters[e].toString(), 12, 12) : op.parameters[e].toString()}
                                                    </span>
                                                  </small>
                                            </span>
                                        )
                                    :
                                        op.parameters &&
                                        <span className="mb-0 d-block">
                                              <small>
                                                parameter:
                                                <span className={op.parameters === this.props.userData.value[this.props.indexer]? "text-primary fw-semi-bold" : "text-secondary fw-semi-bold"}>
                                                  &nbsp; {op.parameters.length > 36? truncStringPortion(op.parameters, 12, 12) : op.parameters}
                                                </span>
                                              </small>
                                        </span>
                                    }
                                </div>}
                              </td>
                              <td className="text-right">
                                <UncontrolledDropdown>
                                  <DropdownToggle
                                    className="btn-icon-only text-light"
                                    href="#pablo"
                                    role="button"
                                    size="sm"
                                    color=""
                                    onClick={(e) => e.preventDefault()}
                                  >
                                    <i className="fas fa-ellipsis-v" />
                                  </DropdownToggle>
                                  <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={() => this.props.copyElement(op.hash)}
                                    >
                                      Copy operation hash / Id
                                    </DropdownItem>
                                    <DropdownItem
                                      href={`${this.props.expAPI}explorer/op/${op.hash}`}
                                      target="_blank"
                                    >
                                      Full stack
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                              </td>
                            </tr>
                        ))}
                      </tbody>
                    </Table>
                }
                {!this.props.fetching && this.props.transactions && this.props.transactions.length === 0 &&
                    <p className="text-secondary ml-3 mt-2">No transactions</p>}
                {this.props.fetching && <span className="h2 font-weight-bold mb-0 text-info ml-4 mb-4">
                        <i className="fas fa-circle-notch fa-spin"></i>
                    </span>}
                {!this.props.fetching && this.props.displayMore &&
                <CardFooter className="py-4 bg-transparent">
                        <Button color="primary" onClick={this.props.fetchMoreOps}>Load more operations</Button>
                    </CardFooter>}
              </Card>
            </div>
          </Row>
      </>
    );
  }
}

export default Operations;
