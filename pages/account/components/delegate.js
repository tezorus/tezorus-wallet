import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  Col,
  Input,
  Row,
} from "reactstrap";
// layout for this page
import * as tezos from 'conseiljs';

const derivationPath = "44'/1729'/0'/0'/0'";

class Delegate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pending: false,
      error: null,
      ledgerKeystore: null,
      password: null,
      receiver: null,
    };
  };

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  updateReceiver = (e) => {
    this.setState({receiver: e.target.value});
  }

  unlockLedger = async () => {
        this.setState({loading: true, error: null});
        try{
            const keystore = await tezos.TezosLedgerWallet.unlockAddress(0, derivationPath);
            this.setState({loading: false, ledgerKeystore: keystore, error: null});
        }catch(error){
            this.setState({loading: false, error: 'Could not connect Ledger device'});
        }
  }

  ledgerSendOp= () => {
        if(!this.state.ledgerKeystore)
            this.setState({error: 'Please connect your ledger first'});
        else{
            this.setState({pending: true, error: null});
            this._asyncRequest = tezos.TezosNodeWriter.sendDelegationOperation(this.props.tezosNodes[0], this.state.ledgerKeystore, this.state.receiver, 30000).then(result => {
                this.setState({pending: false});
                this.props.renderSuccessMsg(`Delegation operation sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.props.userData.value[this.props.indexer]}</strong>`);
                this.props.toggleDisplayDelegate()
            })
            .catch(error => {
                this.setState({error: error, pending: false});
            });
        }
    }

  keystoreSendOp= () => {
        if(!this.props.userData.type[this.props.indexer] === 'file')
            this.setState({error: 'Please select a Keystore wallet file'});
        else if(!this.state.password)
            this.setState({error: 'Missing parameters'});
        else{
            this.setState({pending: true, error: null});
            this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(this.props.userData.keystore[this.props.indexer]), this.state.password).then(loadedWallet => {
                console.log(this.props.tezosNodes[0]);
                this._asyncRequest = tezos.TezosNodeWriter.sendDelegationOperation(this.props.tezosNodes[0], loadedWallet.identities[0], this.state.receiver, 30000).then(result => {
                    this.setState({pending: false});
                    this.props.renderSuccessMsg(`Delegation operation sent with ID <small>${result.operationGroupID.replace(/['"]+/g, '')}</small> for <strong>${this.props.userData.value[this.props.indexer]}</strong>`);
                    this.props.toggleDisplayDelegate()
                })
                .catch(error => {
                    this.setState({error: error.toString(), pending: false});
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({error: 'Invalid password', pending: false});
            });
        }
    }

  render() {
    return (
        <Row className="card-operation">
            <Col md="3" className="d-none d-md-block"></Col>
            <Col xs="12" md="6">
                <Card className="card-stats card-blur mt-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h2"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Delegate funds
                          </CardTitle>
                          <span className="text-white d-block text-sm">Available funds :
                              <span className="font-weight-bold mb-0 text-yellow shadow-sm ml-2 text-md">
                                {this.props.balance} ꜩ
                              </span>
                          </span>
                          <hr/>
                          <span className="text-white d-block text-sm mb-2">
                                Delegation to baking nodes, can reward you with ꜩ based on your wallet balance.
                                You can find more information on Baking Bad <a className="link" target="_blank" href="https://baking-bad.org/docs/tezos-staking-for-beginners">documentation</a>.
                          </span>
                          <span className="text-white d-block text-sm mb-2">
                                We advice you to pick a baking node from <a className="link" target="_blank" href="https://baking-bad.org/">Baking Bad</a> reference list.
                          </span>
                          <Input type="text" placeholder="tz1/KT1 baker" className="ml-1 border-0"
                            onChange={(e) => this.updateReceiver(e)}/>
                          <span className="text-white d-block text-sm mt-2">
                            0.03 ꜩ fees
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-primary rounded-circle shadow">
                            <i className="fas fa-hand-holding-usd"></i>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                      {this.state.error && <span className="text-danger d-block ml-2 mt-2">
                           <small>{this.state.error}</small>
                        </span>}
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'file' &&
                            <Col md="12">
                                <p className="mt-3 mb-1 text-secondary text-sm">Password</p>
                                <Input type="password" placeholder="Password to unlock keystore file" className="ml-1 border-0"
                                onChange={(e) => this.updatePassword(e)}/>
                            </Col>
                        }
                        {this.state.ledgerKeystore && <span className="text-yellow d-block ml-2 mt-2">
                           <small>{this.state.ledgerKeystore.publicKeyHash} connected</small>
                        </span>}
                      </Row>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        {this.props.userData && this.props.userData.type[this.props.indexer] === 'ledger' && !this.state.ledgerKeystore && !this.state.loading &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2"
                                              onClick={this.unlockLedger}>
                                        Connect Ledger
                            </Button>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.loading &&
                            <small className="text-white">Follow instructions on your Ledger...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'ledger' && this.state.ledgerKeystore && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.ledgerSendOp()}}>Delegate</Button>
                        }
                        {this.state.pending &&
                            <small className="text-secondary">Sending delegation operation...</small>
                        }
                        {this.props.userData.type[this.props.indexer] === 'file' && !this.state.pending &&
                            <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={() => {this.keystoreSendOp()}}>Delegate</Button>
                        }

                        <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right"  onClick={() => {this.props.toggleDisplayDelegate()}}>Cancel</Button>
                    </CardFooter>
                  </Card>
            </Col>
            <Col md="3" className="d-none d-md-block"></Col>
        </Row>
    );
  }
}

export default Delegate;
