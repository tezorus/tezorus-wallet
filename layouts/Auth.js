import React from "react";
// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import AuthFooter from "components/Footers/AuthFooter.js";
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import {getJWT} from '../utils/user.js';
import logoFabrick from '../assets/img/brand/logo-white.png';

class Auth extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
            active: false
        };
  }

  componentDidMount = () => {
    document.body.classList.add("bg-default");
    const jwt = getJWT();
    if(jwt)
        this.setState({active: true});
  };


  render() {
    return (
        <>
          {!this.state.active?
              <>
                  <div className="main-content">
                    <div className="header py-5">
                      <Container>
                        <div className="header-body text-center mb-2">
                          <Row className="justify-content-center">
                            <Col lg="6" md="8">
                              <img src={logoFabrick} width="140px"/>
                              <small className="d-block text-white bg-dark mt-3 mb-3 p-1 rounded">Currently in BETA version. Use for trial only. 
                              <br/>Please contact us on <a href="https://t.me/tezorus" target="_blank">Telegram</a> or <a href="https://discord.gg/bcevXG8CvU" target="_blank">Discord</a> for bug reports.</small>
                              <h2 className="mt-3 text-white">Access your personalized Dashboard</h2>
                            </Col>
                          </Row>
                        </div>
                      </Container>
                    </div>
                    {/* Page content */}
                    <Container className="mt--10 pb-5">
                      <Row className="justify-content-center">{this.props.children}</Row>
                    </Container>
                  </div>
                  <AuthFooter />
              </>
          :
              <>
                  <div className="main-content">
                    <AccountNavbar {...this.props} test={this.props.children.indexer} />
                    {this.props.children}
                  </div>
                  <AccountFooter />
              </>
          }
        </>
    );
  }
}

export default Auth;
