import React from "react";
import Link from "next/link";

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  Input,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Create from "layouts/Create.js";
import * as tezos from 'conseiljs';

const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})");
const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");
const weakRegex = new RegExp("^(?=.{4,})");

class CreateWallet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        mnemonic: '',
        password: '',
        walletData: null,
        publicKeyHash: null,
        pending: false
    };
  }

  componentDidMount = () => {
    this.generateMnemonic();
  }

  changeMnemonic = (e) => {
    this.setState({mnemonic: e.target.value});
  }

  updatePswd = (e) => {
    this.setState({password: e.target.value});
  }

  generateMnemonic = () => {
    const mnemonic = tezos.TezosWalletUtil.generateMnemonic();
    this.setState({mnemonic: mnemonic});
  }

  generateKeys = async () => {
    this.setState({pending: true});
    const keystore = await tezos.TezosWalletUtil.unlockIdentityWithMnemonic(this.state.mnemonic, '');
    const newWallet = await tezos.TezosFileWallet.saveWalletString({identities: [keystore]}, this.state.password);
    this.setState({
        walletData: "text/json;charset=utf-8," + encodeURIComponent(newWallet),
        publicKeyHash: keystore.publicKeyHash,
        pending: false
    });
  }

  render() {
    return (
      <>
        <Col lg="7" md="11" xs="12">
          <Card className="shadow border-0 mt-2 mb-2 card-blur">
            <CardHeader className="bg-transparent">
              <div className="text-secondary text-center mt-2 mb-1">
                <h1 className="text-white"><big>+<i className="fa fa-wallet"></i></big></h1>
                <h3 className="text-yellow">Create new Wallet</h3>
                <p>Generate a new secured keystore file to store your wallet key</p>
              </div>
            </CardHeader>
            <CardBody className="mt-1">

              <p className="text-secondary">
                1.
                <strong className="ml-1 mr-1 text-info cursor-pointer" href="" onClick={() => {this.generateMnemonic()}}>
                    Generate random Seed words
                </strong>
                or set existing ones to restore your wallet
              </p>
              <Input type="textarea" placeholder="Seed words" className="ml-2" value={this.state.mnemonic} onChange={this.changeMnemonic}/>
              <p className="text-secondary"><small>
                It is strongly advised to save in a secure place your seed words to restore your wallet
              </small></p>
              <p className="mt-3 text-secondary">2. Set your password to unlock your wallet</p>
              <Row>
                <Col xs={8} lg={10}>
                    <Input type="password" className="ml-2 border-0" placeholder="Password" value={this.state.password} onChange={this.updatePswd}/>
                </Col>
                <Col xs={4} lg={2}>
                    {strongRegex.test(this.state.password)?
                        <Badge color={'success'}>
                          Strong
                          <small className="d-block">password</small>
                        </Badge>
                    :
                        mediumRegex.test(this.state.password)?
                            <Badge color={'warning'}>
                              Medium
                              <small className="d-block">password</small>
                            </Badge>
                        :
                             weakRegex.test(this.state.password) &&
                                 <Badge color={'danger'}>
                                    Weak
                                    <small className="d-block">password</small>
                                 </Badge>
                    }
                </Col>
                <Col xs={12} lg={12}>
                    <p className="text-secondary"><small>A strong password is min. 10 char, with numbers, capital letter and special character</small></p>
                </Col>
              </Row>
              {this.state.pending?
                  <p className="mt-3">Processing... Please wait</p>
                  :
                  <div className="mt-3">
                    {weakRegex.test(this.state.password)?
                            !this.state.walletData &&
                            <Button className="mb-2" color="primary" onClick={() => {this.generateKeys()}}>
                                Create keystore file
                            </Button>
                        :
                            !this.state.walletData &&
                            <Button className="mb-2" color="inverse" disabled>
                                Create keystore file
                            </Button>
                    }
                    {this.state.walletData?
                        <div>
                            <a href={`data:${this.state.walletData}`} className="text-white bg-success p-2 rounded w-100 h-100"
                               download={`${this.state.publicKeyHash}.tezwallet`}>
                                <small className="fw-semi-bold">Download {this.state.publicKeyHash} wallet</small>
                            </a>
                            <p className="mt-3 text-white">
                                You can now download your keystore file and
                                use it to <Link href="/auth/home">access the dashboard</Link>
                            </p>
                        </div>
                        :
                        <Link href="/auth/home"><Button className="float-right mb-2" color="inverse">
                            Cancel
                        </Button></Link>
                    }
                  </div>
              }
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

CreateWallet.layout = Create;

export default CreateWallet;
