# Tezorus Web Wallet

A full web app version of tezOrus wallet.  
New desktop and mobile versions will come soon.  
tezOrus app will be the easiest and most accessible way to manage and operate your funds and FA tokens.

## Description

A web based platform to interact with Tezos mainnet and testnets, to read, send and operate FA tokens.
Tezorus relies onto a custom version of ConseilJS; enabling rpc interaction and Ledger device connection.
The platform is full front nodeJS, based on [NextJS](https://nextjs.org/) framework.
Project relies on the MIT licenced version of [Argon-ReactJS-Dashboard](https://github.com/creativetimofficial/argon-dashboard-react) by CreativeTim

## Requirements

NodeJS + npm

## Quick start

`npm install` to install node modules

`npm run dev` to run dev. environment

## Deployment
#### clone repository
```
git clone https://gitlab.com/tezorus/tezorus-wallet.git
```

#### Install Libudev (for hardwallet connexion)
```
sudo apt install libudev-dev
```

#### Install NodeJS
```
sudo apt install nodejs
```

#### Install npm
```
sudo apt install npm
```

#### Check NodeJS version
```
nodejs -v
```

If installed NodeJS version is lower than 10.11, force install version 10.11:
```
sudo npm cache clean -f
sudo npm install -g n
sudo n 10.11
```
npm version should be >= 6.4.1

#### Install node modules and build project
```
npm install
npm run-script build
```

#### start 
```
npm start
```

#### nginx config file
```
sudo nano /etc/nginx/sites-enabled/app-test.tezorus.com
```
Add th following content to the nginx config file:
```
server {
        listen 80;
        listen [::]:80;

        server_name app-test.tezorus.com www.app-test.tezorus.com;

        access_log /var/log/nginx/app-test.tezorus.access.log;
        error_log /var/log/nginx/app-test.tezorus.error.log;

        location / {
                proxy_pass http://127.0.0.1:3000;
        }
}
```
Restart nginx:
```
sudo service nginx restart
```

#### Create ssl certificate and auto forward to https port
```
sudo certbot --nginx -d app-test.tezorus.tech -d www.app-test.tezorus.tech
```

Install cerbot if not installed:
```
sudo apt install python-certbot-nginx
```

And restart nginx


#### Create service config file
Create a systemd service file:
```
sudo nano /etc/systemd/system/tezorus.service
```

And insert the following content:
```
[Unit]
Description=NodeJS server to run tezOrus wallet
Documentation=https://app-test@tezorus.com
After=network.target

[Service]
Environment=NODE_PORT=3000
Type=simple
User=tezorus
ExecStart=/usr/bin/npm start --prefix /home/tezorus/tezorus-wallet/
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

To automatically start when server reboots:
```
sudo systemctl enable tezorus
```

## Update project on server

```
rm package-lock.json
git pull origin master
npm install
sudo rm -r .next/
npm run-script build
sudo service tezorus restart
```
