import React from "react";
import axios from "axios";
// reactstrap components
import { Button, 
    Container, 
    Card, 
    CardBody, 
    CardFooter, 
    CardTitle, 
    Col,
    Input, 
    Row } from "reactstrap";
// layout for this page
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import {getJWT, logOut} from '../../utils/user.js';

import { FABRICK_EXP } from '../../utils/constants.js';

const derivationPath = "44'/1729'/0'/0'/0'";

class Contract extends React.Component {
  static getInitialProps ({ query: { address, network } }) {
    return { address: address, network: network }
  }

  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      password: null,
      oldPassword: null,
      newPassword: null,
      error: null,
      success: null,
      delete: false,
      deactivate: false,
    };
    this.hiddenFileInput = React.createRef();
  };

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  updateOldPassword = (e) => {
    this.setState({oldPassword: e.target.value});
  }

  updateNewPassword = (e) => {
    this.setState({newPassword: e.target.value});
  }

  setNewPassword = () => {
    this.setState({error: null, success: null, pending: true})  
    if(this.state.newPassword.length < 8)
        this.setState({error: 'Invalid password format. Must be 8 characters minimum'})    
    else{
        const jwt = getJWT();
        this._asyncRequest = axios.post(`${FABRICK_EXP.replace('tezos/', '')}account/password`, 
            {current_password: this.state.oldPassword, password: this.state.newPassword},
            {headers: {'X-AUTH-USER': jwt}}).then(
                resp => {
                    if(resp.data.status){
                        this.setState({success: 'Password updated', pending: false});
                    }else{
                        this.setState({error: resp.data.error, pending: false});
                    }
                }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not update password. Wrong credentials.', pending: false});
        });
    }
  }

  deleteAccount = () => {
    const jwt = getJWT();
        this._asyncRequest = axios.post(`${FABRICK_EXP.replace('tezos/', '')}account/deactivate`, {password: this.state.password},
            {headers: {'X-AUTH-USER': jwt}}).then(
                resp => {
                    console.log(JSON.stringify(resp));
                    if(resp.data.status){
                        this.setState({success: 'Account successfully deleted', delete: true});
                        setTimeout(
                            function() {
                                logOut();
                            }
                            .bind(this),
                            2000
                        );
                    }else{
                        this.setState({error: resp.data.error});
                    }
                }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not delete account'});
        });
  }

  componentDidMount = () => {
    if(!getJWT())
        window.location.href = '/';
  };

  render() {
    return (
        <div className="main-content">
            <AccountNavbar expUrl={this.state.expUrl} />
            <Container className="mt-3" fluid style={{minHeight: "calc(100vh - 100px)"}}>
            <Row className="card-operation mb-4">
                <Col md="3" className="d-none d-md-block"></Col>
                <Col xs="12" md="6">
                    <Card className="card-stats card-blur mt-5">
                        <CardBody>
                        <Row>
                                <div className="col">
                                    <CardTitle
                                        tag="h2"
                                        className="text-uppercase text-secondary mb-2"
                                    >
                                        Account settings
                                    </CardTitle>
                                </div>
                                <Col className="col-auto">
                                    <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                                        <i className="fas fa-cogs"></i>
                                    </div>
                                </Col>
                        </Row>
                        {this.state.error && <small className="d-block bg-danger font-size-10 rounded text-white mt-2 p-1 pl-3">{this.state.error}</small>}
                        {this.state.success && <small className="d-block bg-success font-size-10 rounded text-white mt-2 p-1 pl-3">{this.state.success}</small>}
                        {!this.state.delete &&
                            <div className="mt-3 mb-2">
                                <p className="mt-3 mb-3 text-primary text-sm"><strong>Update password</strong></p>
                                <small className="d-block mt-2 text-secondary ">Current password</small>
                                <Input type="password" placeholder="Current password" className="ml-1 border-0"
                                                        onChange={(e) => this.updateOldPassword(e)}/>
                                <small className="d-block mt-2 text-secondary ">New password</small>
                                <Input type="password" placeholder="New password" className="ml-1 border-0"
                                                        onChange={(e) => this.updateNewPassword(e)}/>
                                <br/>
                                {!this.state.pending && <Button size="sm" color="primary" className="bg-primary border-0 p-2 d-block mt-3" onClick={this.setNewPassword}>Update password</Button>}
                                {this.state.pending &&
                                    <small className="text-secondary">Please wait...</small>
                                }
                            </div>
                        }
                        </CardBody>
                        {!this.state.delete && <CardFooter className="bg-transparent text-white">
                            {this.state.deactivate?
                            <>
                                <Input type="password" placeholder="Password to delete account" className="ml-1 border-0"
                                                                onChange={(e) => this.updatePassword(e)}/>
                                <br/>
                                <Button size="sm" color="primary" className="bg-danger border-0 p-2 text-white d-block mt-2" onClick={() => {if(window.confirm('Delete your account permanently? All saved information & wallets will be lost.')){this.deleteAccount()};}}>Click to delete account</Button>
                            </>
                            :
                                <Button size="sm" color="primary" className="border-0 p-2" onClick={() => {this.setState({deactivate: true})}}>Delete account</Button>
                            }
                            <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right" onClick={() => {window.location.href="/account/dashboard"}}>Close settings</Button>
                        </CardFooter>}
                    </Card>
                </Col>
                <Col md="3" className="d-none d-md-block"></Col>
            </Row>
            <br/>
            </Container>
            <AccountFooter />
        </div>
    );
  }
}

export default Contract;
