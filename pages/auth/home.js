import React from "react";
import Router from 'next/router'
import Link from "next/link";
import axios from "axios";
import {storeAuth, loadAuth, getJWT} from '../../utils/user.js';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Container,
  Input,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";
import * as tezos from 'conseiljs';

const derivationPath = "44'/1729'/0'/0'/0'";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        selectedOption: null,
        errorFile: false,
        encryptedWallet: null,
        fileName: null,
        publicAddress: '',
        password: '',
        error: null,
        loading: false,
        ledgerKeystore: null,
        keystorePub: null,
        encryptedWallet: null,
        active: false,
        userData: null,
        expFabrick: 'https://explorer-test.fabrick.tech/api/v1/account/',
        jwt: null,
        alias: '',
    };
    this.hiddenFileInput = React.createRef();
  }

  componentDidMount = () => {
    this.setState({jwt: getJWT(), userData: loadAuth()});
  };

  handleUpload = (event) => {
    this.hiddenFileInput.current.click();
  };

  onFileHandler = (event) => {
    const reader = new FileReader();
    reader.addEventListener('load', event => {
        try{
            const data = event.target.result.split(';base64,').pop();
            const buff = new Buffer(data, 'base64');
            const jsonData = JSON.parse(buff.toString('ascii'));
            this.setState({
                encryptedWallet: jsonData,
                errorFile: false
            });
        }catch(error){
            console.log(error);
            this.setState({errorFile: true});
        }
    });
    this.setState({fileName: event.target.files[0].name});
    reader.readAsDataURL(event.target.files[0]);
  }

  updatePublicAddress = (e) => {
    this.setState({publicAddress: e.target.value});
  }

  updateAlias = (e) => {
    this.setState({alias: e.target.value});
  }

  loadAddress = () => {
      if(!this.state.publicAddress || this.state.publicAddress.length !== 36)
        this.setState({error: 'Invalid public address format'});
      else if(!this.state.alias || this.state.alias.length < 3)
        this.setState({error: 'Invalid wallet name'});
      else{
        this.setState({loading: true, error: null});
        const keystoreAuth = {
            type: ["address"],
            value: [this.state.publicAddress],
            keystore: [{}],
            alias: [this.state.alias]
        }
        if(storeAuth(keystoreAuth)){
            this.saveWallet(keystoreAuth);
        }else
            this.setState({loading: false, error: 'Unexpected error occurred'});
      }
  }

  updatePswd = (e) => {
    this.setState({password: e.target.value});
  }

  unlockKeys = async () => {
    this.setState({loading: true, error: null});
    try{
        const loadedWallet = await tezos.TezosFileWallet.loadWalletString(JSON.stringify(this.state.encryptedWallet), this.state.password);
        this.setState({loading: false, error: null, keystorePub: loadedWallet.identities[0].publicKeyHash});
    }catch(error){
        console.log(error);
        this.setState({error: 'Wrong file or password', loading: false});
    }
  }

  unlockLedger = async () => {
        this.setState({loading: true, error: null});
        try{
            const keystore = await tezos.TezosLedgerWallet.unlockAddress(0, derivationPath);
            this.setState({loading: false, ledgerKeystore: keystore, error: null});
        }catch(error){
            this.setState({loading: false, error: 'Could not connect Ledger device'});
        }
  }

  connectLedgerDashboard = () => {
        if(!this.state.alias || this.state.alias.length < 3)
            this.setState({error: 'Invalid wallet name'});
        else{
            this.setState({loading: true, error: null});
            const keystoreAuth = {
                type: ["ledger"],
                value: [this.state.ledgerKeystore.publicKeyHash],
                keystore: [this.state.ledgerKeystore],
                alias: [this.state.alias]
            }
            if(storeAuth(keystoreAuth)){
                this.saveWallet(keystoreAuth);
            }else
                this.setState({loading: false, error: 'Unexpected error occurred'});
        }
  }

  connectDashboard = () => {
        if(!this.state.alias || this.state.alias.length < 3)
            this.setState({error: 'Invalid wallet name'});
        else{
            this.setState({loading: true, error: null});
            const keystoreAuth = {
                type: ["file"],
                value: [this.state.keystorePub],
                keystore: [this.state.encryptedWallet],
                alias: [this.state.alias]
            }
            if(storeAuth(keystoreAuth)){
                this.saveWallet(keystoreAuth);
            }
            else
                this.setState({loading: false, error: 'Unexpected error occurred'});
        }
  }

  saveWallet = (keystore) => {
    if(this.state.jwt){
        this._asyncRequest = axios.post(`${this.state.expFabrick}wallet/address`, {
                address: keystore.value[0],
                wallet_type: keystore.type[0],
                blockchain: "tezos",
                alias: keystore.alias[0]
            },
            {headers: {'X-AUTH-USER': this.state.jwt}}).then(
                resp => {
                    Router.push('/account/dashboard');
                }
        ).catch(error => {
            console.log(error);
            this.setState({error: 'Could not load addresses', pending: false});
            Router.push('/account/dashboard');
        });
    }else
      Router.push('/account/dashboard');
  }

  render() {
    return (
      !this.state.selectedOption?
          <Container fluid>
          <Row>
            <Col lg="12">
                <p className="text-secondary text-center">
                    Do not have a wallet yet?
                    <Link href="/auth/createwallet">
                        <strong className="ml-2 cursor-pointer text-white">Create wallet</strong>
                    </Link>
                </p>
            </Col>
            {this.state.jwt?
                !this.state.userData?
                <Col lg="3" md="6" xs="12">
                  <Card className="bg-gradient-danger text-white shadow border-0 mt-2 mb-2">
                    <CardHeader className="bg-transparent">
                      <div className="text-white text-center mt-2 mb-1">
                        <h1 className="text-white"><big><i className="far fa-user"></i></big></h1>
                        <h3 className="text-white">Signed In</h3>
                        <p className="text-white">No wallet referenced</p>
                      </div>
                    </CardHeader>
                    <CardBody className="mt-1">
                      <div className="text-center text-white mb-4">
                        <small>
                            You are successfully logged in, but do not have any wallet yet.
                            Set a wallet to access your personalized dashboard
                            <br/>
                            Do not have a wallet?
                            <Link href="/auth/createwallet">
                                <big className="fw-semi-bold text-white ml-2 cursor-pointer">Create wallet</big>
                            </Link>
                        </small>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                :
                <Col lg="3" md="6" xs="12">
                  <Card className="bg-gradient-info text-white shadow border-0 mt-2 mb-2">
                    <CardHeader className="bg-transparent">
                      <div className="text-white text-center mt-2 mb-1">
                        <h1 className="text-white"><big>+ <i className="fas fa-wallet"></i></big></h1>
                        <h3 className="text-white">Create a new Wallet</h3>
                        <p className="text-white">Get a new keystore file</p>
                      </div>
                    </CardHeader>
                    <CardBody className="mt-1">
                      <div className="text-center text-white mb-4">
                        <small>
                            Set and back up seed words to create a new encrypted Keystore file
                            <Link href="/auth/createwallet">
                                <big className="fw-semi-bold text-white ml-2 cursor-pointer d-block text-center mt-3">Create wallet</big>
                            </Link>
                        </small>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
            :
                <Col lg="3" md="6" xs="12">
                  <Card className="bg-gradient-info text-white shadow border-0 mt-2 mb-2">
                    <CardHeader className="bg-transparent">
                      <div className="text-white text-center mt-2 mb-1">
                        <h1 className="text-white"><big><i className="fa fa-user-circle"></i></big></h1>
                        <h3 className="text-white">Register an account</h3>
                        <p className="text-white">Access, save and retrieve your addresses </p>
                      </div>
                    </CardHeader>
                    <CardBody className="mt-1">
                      <div className="text-center text-white mb-4">
                        <small>
                            All saved public addresses will saved anytime once logged in.
                            <br/>
                            Already registered? <br/>
                            <Link href="/auth/login">
                                <big className="ml-2 cursor-pointer"><strong className="text-primary">Sign in</strong></big>
                            </Link>
                        </small>
                      </div>
                      <div className="text-center">
                          <Link href="/auth/register">
                          <Button className="my-4" color="primary" type="button">
                            Register
                          </Button>
                          </Link>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
            }
            <Col lg="3" md="6" xs="12">
              <Card className="shadow border-0 mt-2 mb-2 position-relative card-blur text-secondary">
                <CardHeader className="bg-transparent">
                  <div className="text-center mt-2 mb-1">
                    <h1 className="text-white"><big><i className="fa fa-wallet"></i></big></h1>
                    <h3 className="text-white">Public address</h3>
                    <p className="text-yellow">Access current balances and operations</p>
                  </div>
                </CardHeader>
                <CardBody className="mt-1">
                  <div className="text-center mb-4">
                    <small>You will access the dashboard in read-only. <br/>
                           You will not be able to send transactions</small>
                  </div>
                  <div className="text-center">
                      <Button className="my-4" color="primary" type="button" onClick={() => {this.setState({selectedOption: "address"})}}>
                        Set public address
                      </Button>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg="3" md="6" xs="12">
              <Card className="shadow border-0 mt-2 mb-2 position-relative card-blur text-secondary">
                <CardHeader className="bg-transparent">
                  <div className="text-center mt-2 mb-1">
                    <h1 className="text-white"><big><i className="fa fa-code-branch"></i></big></h1>
                    <h3 className="text-white">Ledger Nano</h3>
                    <p className="text-yellow">Access current balances and send transactions</p>
                  </div>
                </CardHeader>
                <CardBody className="mt-1">
                  <div className="text-center mb-4">
                    <small>
                        All dashboard features will be accessible.
                        Your Ledger key secures all transfers and operations.
                    </small>
                  </div>
                  <div className="text-center">
                      <Button className="my-4" color="primary" type="button" onClick={() => {this.setState({selectedOption: "ledger"})}}>
                        Connect Ledger
                      </Button>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg="3" md="6" xs="12">
              <Card className="shadow border-0 mt-2 mb-2 position-relative card-blur text-secondary">
                <CardHeader className="bg-transparent">
                  <div className="text-primary text-center mt-2 mb-1">
                    <h1 className="text-white"><big><i className="fa fa-file-signature"></i></big></h1>
                    <h3 className="text-white">Keystore file</h3>
                    <p className="text-yellow">Access current balances and send transactions</p>
                  </div>
                </CardHeader>
                <CardBody className="mt-1">
                  <div className="text-center mb-4">
                    <small>
                        All dashboard features will be accessible.
                        Your Keystore file will be loaded and secured in your browser to send operations.
                    </small>
                  </div>
                  <div className="text-center">
                      <Button className="my-4" color="primary" type="button"  onClick={() => {this.setState({selectedOption: "keystore"})}}>
                        Upload Keystore file
                      </Button>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          </Container>
      :
        <Container fluid>
        <Row>
            {this.state.selectedOption === "address" &&
                <>
                <Col lg="3" md="2" xs="12">
                </Col>
                <Col lg="6" md="9" xs="12">
                  <Card className="shadow border-0 card-blur text-secondary">
                    <CardHeader className="bg-transparent">
                      <div className="text-center mt-2 mb-1">
                        <h1 className="text-white"><big><i className="fa fa-wallet"></i></big></h1>
                        <h3 className="text-white">Public address</h3>
                        <strong className="text-yellow">Access current balances and sent operations</strong>
                      </div>
                    </CardHeader>
                    <CardBody className="mt-1">
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="tz1..."
                          type="text"
                          value={this.state.publicAddress}
                          onChange={(e) => this.updatePublicAddress(e)}
                        />
                      </InputGroup>
                      <div className="text-center mb-4">
                        <small>You will only access the dashboard in read-only
                               i.e. you will not be able to send transactions</small>
                      </div>
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="Alias / Wallet name"
                          type="text"
                          value={this.state.alias}
                          onChange={(e) => this.updateAlias(e)}
                        />
                      </InputGroup>
                      <small>Set a wallet name for an easier management</small>
                      {this.state.error &&
                         <p className="text-danger mt-1 mb-2 text-center">Wrong file format</p>
                      }
                      {this.state.loading?
                              <p className="text-info mt-1 mb-2 text-center"><small>Processing...</small></p>
                          :
                              <div className="text-center">
                                  <Button className="my-4 mr-2" color="secondary" type="button"
                                          onClick={() => {this.setState({selectedOption: null})}}>
                                    Cancel
                                  </Button>
                                  <Button className="my-4 m-2" color="primary" type="button"
                                          onClick={() => {this.loadAddress()}}>
                                    Set public address
                                  </Button>
                              </div>
                      }
                    </CardBody>
                  </Card>
                </Col>
                </>
            }
            {this.state.selectedOption === "ledger" &&
                <>
                <Col lg="3" md="2" xs="12">
                </Col>
                <Col lg="6" md="9" xs="12">
                  <Card className="shadow border-0 card-blur text-secondary">
                    <CardHeader className="bg-transparent">
                      <div className="text-center mt-2 mb-1">
                        <h1 className="text-white"><big><i className="fa fa-code-branch"></i></big></h1>
                        <h3 className="text-white">Ledger Nano</h3>
                        <strong className="text-yellow">Access current balances and send transactions</strong>
                      </div>
                    </CardHeader>
                    <CardBody className="mt-1">
                      <div className="text-left mb-4 pl-4">
                        1. <small>Plug your ledger to your computer</small><br/>
                        2. <small>Unlock your Ledger</small><br/>
                        3. <small>Select the wallet you want to connect</small><br/>
                      </div>
                      {this.state.error &&
                         <p className="text-danger mt-1 mb-2 text-center">{this.state.error}</p>
                      }
                      {this.state.loading?
                              <p className="text-info mt-1 mb-2 text-center">
                                <small>Please follow instructions from your Ledger Nano</small>
                              </p>
                          :
                              !this.state.ledgerKeystore?
                                  <div className="text-center">
                                      <Button className="my-4 mr-2" color="secondary" type="button"
                                              onClick={() => {this.setState({selectedOption: null})}}>
                                        Cancel
                                      </Button>
                                      <Button className="my-4 m-2" color="primary" type="button"
                                              onClick={this.unlockLedger}>
                                        Connect Ledger
                                      </Button>
                                  </div>
                              :
                                  <div className="text-center">
                                      <p className="text-info mb-2">
                                        <small>{this.state.ledgerKeystore.publicKeyHash} connected</small>
                                      </p>
                                      <InputGroup className="input-group-alternative">
                                        <Input
                                          placeholder="Alias / Wallet name"
                                          type="text"
                                          value={this.state.alias}
                                          onChange={(e) => this.updateAlias(e)}
                                        />
                                      </InputGroup>
                                      <small className="d-block mb-4">Set a wallet name for an easier management</small>
                                      <Button className="my-4 m-2" color="success" type="button"
                                                  onClick={() => {this.connectLedgerDashboard()}}>
                                        Connect Dashboard
                                      </Button>
                                  </div>
                      }
                    </CardBody>
                  </Card>
                </Col>
                </>
            }
            {this.state.selectedOption === "keystore" &&
                <>
                    <Col lg="3" md="2" xs="12">
                    </Col>
                    <Col lg="6" md="8" xs="12">
                      <Card className="shadow border-0 card-blur text-secondary">
                        <CardHeader className="bg-transparent">
                          <div className="text-primary text-center mt-2 mb-1">
                            <h1 className="text-white"><big><i className="fa fa-file-signature"></i></big></h1>
                            <h3 className="text-white">Keystore file</h3>
                            <strong className="text-yellow">Access current balances and send transactions</strong>
                          </div>
                        </CardHeader>
                        <CardBody className="mt-1">
                          <div className="text-center mt-3 mb-2">
                            <small>Select your Keystore file (usually .tezwallet)</small>
                          </div>
                          <input
                              placeholder="Keystore file"
                              type="file" className="d-none"
                              ref={this.hiddenFileInput}
                              onChange={(e) => this.onFileHandler(e)}
                          />
                          <div className="text-center">
                              {!this.state.encryptedWallet?
                                  <Button color="primary" type="button" onClick={this.handleUpload}>
                                      Upload Keystore file
                                  </Button>
                              :
                                 <div>
                                    <strong className="text-yellow mb-1">
                                        <small><strong>{this.state.fileName}</strong> selected</small>
                                    </strong>
                                    <p className="text-info cursor-pointer mt-1" onClick={() => {this.setState({encryptedWallet: null})}}>
                                        <small>Click to change file</small>
                                    </p>
                                 </div>
                              }
                              {this.state.errorFile &&
                                 <p className="text-danger">Wrong file format</p>
                              }
                          </div>
                          <div className="text-center mt-3 mb-1">
                            <small>Set your passphrase / password to unlock your keystore</small>
                          </div>
                          <InputGroup className="input-group-alternative">
                            <Input
                              placeholder="Password"
                              type="password"
                              value={this.state.password} onChange={this.updatePswd}
                            />
                          </InputGroup>
                          {this.state.error &&
                             <p className="text-danger mt-1 mb-2 text-center">{this.state.error}</p>
                          }
                          {this.state.loading?
                                  <p className="text-info mt-1 mb-2 text-center"><small>Processing...</small></p>
                              :
                                  !this.state.keystorePub?
                                      <div className="text-center">
                                          <Button className="my-4 mr-2" color="secondary" type="button"
                                                  onClick={() => {this.setState({selectedOption: null})}}>
                                            Cancel
                                          </Button>
                                          <Button className="my-4 m-2" color="primary" type="button"
                                                  onClick={this.unlockKeys}>
                                            Unlock keystore file
                                          </Button>
                                      </div>
                                  :
                                      <div className="text-center">
                                          <small className="text-success d-block mt-3 mb-2">
                                            <strong>{this.state.keystorePub}</strong> connected
                                          </small>
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Alias / Wallet name"
                                              type="text"
                                              value={this.state.alias}
                                              onChange={(e) => this.updateAlias(e)}
                                            />
                                          </InputGroup>
                                          <small className="d-block mb-4">Set a wallet name for an easier management</small>
                                          <Button className="my-4 m-2" color="success" type="button"
                                                      onClick={() => {this.connectDashboard()}}>
                                            Connect Dashboard
                                          </Button>
                                      </div>
                          }
                        </CardBody>
                      </Card>
                    </Col>
                </>
            }
        </Row>
        </Container>
    );
  }
}

Home.layout = Auth;

export default Home;
