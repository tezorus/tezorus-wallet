import React from "react";
import axios from "axios";
import Router from "next/router";
// reactstrap components
import { Button, 
    Container, 
    Card, 
    CardBody, 
    CardFooter, 
    CardTitle, 
    Col,
    Dropdown,
    DropdownMenu,
    DropdownItem,
    DropdownToggle,
    Input, 
    Row } from "reactstrap";
// layout for this page
import AccountNavbar from "components/Navbars/AccountNavbar.js";
import AccountFooter from "components/Footers/AccountFooter.js";
import {loadAuth, storeAuth, selectWallet, saveContract} from '../../utils/user.js';
import {truncStringPortion} from '../../utils/helper.js';

import { FABRICK_EXP } from '../../utils/constants.js';
import * as tezos from 'conseiljs';

const derivationPath = "44'/1729'/0'/0'/0'";

class Contract extends React.Component {
  static getInitialProps ({ query: { address, network } }) {
    return { address: address, network: network }
  }

  constructor(props) {
    super(props);
    this.state = {
      indexer: 0,
      userData: null,
      address: null,
      network: "mainnet",
      networks: ['mainnet'],
      expFabrick: `${FABRICK_EXP}mainnet/`,
      tezosNodes: ['https://mainnet.smartpy.io/'],
      copied: false,
      dropdownOpen: false,
      dropdownEntrypointsOpen: false,
      contractAddr: '',
      error: null,
      success: null,
      entryPoints: null,
      selectedEntrypoint: null,
      password: null,
      encryptedWallet: null,
      parameters: null,
      entries: [],
      burnTez: 0, 
      feesTez: 0, 
      storageCost: 0, 
      callParams: null, 
      ledgerPending: false,
      keystorePending: false,
      ledgerKeystore: null,
      fileName: null,
      copied: false,
    };
    this.hiddenFileInput = React.createRef();
  };

  updatePassword = (e) => {
    this.setState({password: e.target.value});
  }

  updateContractAddr = (e) => {
    this.setState({contractAddr: e.target.value});
  }

  processCopy = () =>{
    navigator.clipboard.writeText(this.state.userData.value[this.state.indexer]);
    this.setState({copied: true});
    setTimeout(
        function() {
            this.setState({copied: false});
        }
        .bind(this),
        2000
    );
  }

  componentDidMount = () => {
    const queries = new URLSearchParams(window.location.search);
    const userData = loadAuth();
    if(userData){
        this.setState({userData: userData,
                       network: queries.get('network') || 'mainnet',
                       indexer: queries.get('indexer') || 0,
                       tezosNodes: [`https://${queries.get('network') || 'mainnet'}.smartpy.io/`],
                       expFabrick: `${FABRICK_EXP}${queries.get('network') || 'mainnet'}/`});
        if(queries.get('contract')){
            this.setState({contractAddr: queries.get('contract')});
            setTimeout(
                () => this.fetchContract(), 
                200
              );
            
        }
            
    }else
        Router.push("/auth/home");
    this._asyncRequest = axios.get(`${this.state.expFabrick}tezorus/config`).then(response => {
        this.setState({networks: response.data.tezos_networks, expAPI: response.data.explorer[0].api});
    })
    .catch(error => {
        console.log(error);
    });
  };

  toggleDropdownEntrypoints = () => {
    this.setState({dropdownEntrypointsOpen: !this.state.dropdownEntrypointsOpen});
  }

  toggleDropdown = () => {
    this.setState({dropdownOpen: !this.state.dropdownOpen});
  }

  toggleNetwork = () => {
    this.setState({dropdownNetwork: !this.state.dropdownNetwork});
  }

  switchNet = (network) => {
    this.setState({network: network,
                       expFabrick: `${FABRICK_EXP}${network}/`, tezosNodes: [`https://${network}.smartpy.io/`]});
  }

  copyElement = (element) => {
    navigator.clipboard.writeText(element);
  }

  switchWallet = (index) => {
    selectWallet(index);
    this.setState({indexer: index});
  }

  fetchContract = () => {
    this.setState({entryPoints: null, selectedEntrypoint: null, error: null});
    this._asyncRequest = axios.get(`${this.state.expFabrick}contract/${this.state.contractAddr}/code`).then(response => {
        const parameters = response.data.contract_code.split('\n').join('').split(';')[0]+';';
        const entryPoints = tezos.TezosContractIntrospector.generateEntryPointsFromParams(parameters), refinedEntrypoints = [];
        for(let entrypoint of entryPoints){
            entrypoint.name = entrypoint.name.split('%safeEntrypoints.').join('');
            refinedEntrypoints.push(entrypoint);
        }
        this._asyncRequest = axios.post(`${this.state.expFabrick}tezorus/custom_contract`, 
            {contract_hash: this.state.contractAddr}).then(
                resp => {
                    console.log(JSON.stringify(resp));
                }
        ).catch(error => {
            console.log(error);
        });
        saveContract(this.state.contractAddr, this.state.network);
        this.setState({entryPoints: refinedEntrypoints, parameters: parameters, error: null});
    })
    .catch(error => {
        console.log(error);
        this.setState({error: `Not found on ${this.state.network} network`});
    });
  }

  updateEntry = (e, name) => {
    let entries = this.state.entries;
    entries[name] = e.target.value;
    this.setState({entries: entries});
  }

  handleUpload = (event) => {
    this.hiddenFileInput.current.click();
  };

  onFileHandler = (event) => {
    const reader = new FileReader();
    reader.addEventListener('load', event => {
        try{
            const data = event.target.result.split(';base64,').pop();
            const buff = new Buffer(data, 'base64');
            const jsonData = JSON.parse(buff.toString('ascii'));
            this.setState({
                encryptedWallet: jsonData,
                error: null
            });
            const keystoreAuth = {
                type: ["file"],
                value: [this.state.userData.value[this.state.indexer]],
                keystore: [jsonData],
                alias: [this.state.userData.alias[this.state.indexer] || 'KeyStore'],
            }
            storeAuth(keystoreAuth);
        }catch(error){
            console.log(error);
            this.setState({error: 'Invalid keystore file'});
        }
    });
    this.setState({fileName: event.target.files[0].name});
    reader.readAsDataURL(event.target.files[0]);
  }

  buildOp = () => {
    const sortObject = o => Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
    const entries = sortObject(this.state.entries);
    let params = [];
    this.state.selectedEntrypoint.parameters.forEach(entry => {
        if(!this.state.entries[entry.name]){
            this.setState({error: `No value for ${entry.name.toString()}`});
            return;
        }
        if(entry.type === 'bool')
            params.push(this.state.entries[entry.name]);
        else if(Number.isNaN(parseFloat(this.state.entries[entry.name])))
            params.push('"'+this.state.entries[entry.name]+'"');
        else
            params.push(parseFloat(this.state.entries[entry.name]));
    });
    const entryPoints = tezos.TezosContractIntrospector.generateEntryPointsFromParams(this.state.parameters);
    var i = 0;
    while(i < entryPoints.length){
        if(entryPoints[i].name.split('%safeEntrypoints.').join('') === this.state.selectedEntrypoint.name)
            break
        i++;
    }
    const callParams = entryPoints[i].generateInvocationPair(...params);
    console.log(callParams);
    if(this.state.userData.type[this.state.indexer] === 'ledger'){
        this.ledgerSimulateOp(callParams);
    }
    else if(this.state.userData.type[this.state.indexer] === 'file'){
        this.keystoreSimulateOp(callParams);
    }
    else
        this.setState({error: 'Read-only address. Cannot send from this wallet'});
  }

  ledgerSimulateOp= (callParams) => {
    this.setState({ledgerPending: true, error: null});
    this._asyncRequest = tezos.TezosLedgerWallet.unlockAddress(0, derivationPath).then(
        keystore => {
            console.log(`SIMULATING ${callParams.entrypoint.split("%safeEntrypoints.").join("")} OP`);
            this._asyncRequest = tezos.TezosNodeWriter.testContractInvocationOperation(this.state.tezosNodes[0], 'main', keystore, this.state.contractAddr, 0, 300000, 2000, 500000, callParams.entrypoint.split("%safeEntrypoints.").join(""), callParams.parameters, tezos.TezosParameterFormat.Michelson).then(
                result => {
                    console.log(JSON.stringify(result));
                    const burnTez = result.gas + 100;
                    const feesTez = Math.round(result.gas/10 + 8000);
                    this.setState({burnTez: burnTez, feesTez: feesTez, storageCost: result.storageCost, callParams: callParams, ledgerPending: false, ledgerKeystore: keystore});
                }
            ).catch(error => {
                console.log(error);
                this.setState({error: 'Simulation could not be processed', ledgerPending: false});
            });
    }).catch(error => {
        console.log(error);
        this.setState({error: 'Simulation could not be processed', ledgerPending: false});
    });     
  }

  keystoreSimulateOp= (callParams) => {
    this.setState({keystorePending: true, error: null});
    const keystore = JSON.stringify(this.state.userData.keystore[this.state.indexer]) === '{}'? this.state.encryptedWallet : this.state.userData.keystore[this.state.indexer];
    this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(keystore), this.state.password).then(
        loadedWallet => {
            const keystoreWallet = loadedWallet.identities[0];
            this._asyncRequest = tezos.TezosNodeWriter.testContractInvocationOperation(this.state.tezosNodes[0], 'main', keystoreWallet, this.state.contractAddr, 0, 300000, 2000, 500000, callParams.entrypoint, callParams.parameters, tezos.TezosParameterFormat.Michelson).then(
                result => {
                    console.log(JSON.stringify(result));
                    const burnTez = result.gas + 100;
                    const feesTez = Math.round(result.gas/10 + 8000);
                    this.setState({burnTez: burnTez, feesTez: feesTez, storageCost: result.storageCost, callParams: callParams, keystorePending: false, ledgerKeystore: keystore});
                }
            ).catch(error => {
                console.log(error);
                this.setState({error: 'Simulation could not be processed', keystorePending: false});
            });
    }).catch(error => {
        console.log(error);
        this.setState({error: 'Invalid keystore paasword', keystorePending: false});
    });     
  }

  sendOperation = () => {
    if(this.state.userData.type[this.state.indexer] === 'ledger'){
        this.setState({ledgerPending: true});
        console.log(`Sending ${this.state.callParams.entrypoint} operation to ${this.state.contractAddr} with fees to ${this.state.feesTez} and burn cap to ${this.state.burnTez}`);
        this._asyncRequest = tezos.TezosNodeWriter.sendContractInvocationOperation(this.state.tezosNodes[0], this.state.ledgerKeystore, this.state.contractAddr, 0, this.state.feesTez, derivationPath, this.state.storageCost, this.state.burnTez, this.state.callParams.entrypoint, this.state.callParams.parameters, tezos.TezosParameterFormat.Michelson).then(
            operation => {
                this.setState({ledgerPending: false});
                this.setState({error: null, success: `Call for <strong>${this.state.callParams.entrypoint}</strong> sent with ID <small>${operation.operationGroupID.replace(/['"]+/g, '')}</small>`});
                window.scrollTo(0, 0);
        }).catch(error => {
            console.log(error);
            this.setState({error: 'Call could not be processed', ledgerPending: false});
        });
    }
    else if(this.state.userData.type[this.state.indexer] === 'file'){
        this.setState({keystorePending: true});
        const keystore = JSON.stringify(this.state.userData.keystore[this.state.indexer]) === '{}'? this.state.encryptedWallet : this.state.userData.keystore[this.state.indexer];
        console.log(`Sending ${this.state.callParams.entrypoint} operation to ${this.state.contractAddr} with fees to ${this.state.feesTez} and burn cap to ${this.state.burnTez}`);
        this._asyncRequest = tezos.TezosFileWallet.loadWalletString(JSON.stringify(keystore), this.state.password).then(
            loadedWallet => {
                const keystoreWallet = loadedWallet.identities[0];
                this._asyncRequest = tezos.TezosNodeWriter.sendContractInvocationOperation(this.state.tezosNodes[0], keystoreWallet, this.state.contractAddr, 0, this.state.feesTez, derivationPath, this.state.storageCost, this.state.burnTez, this.state.callParams.entrypoint, this.state.callParams.parameters, tezos.TezosParameterFormat.Michelson).then(
                    operation => {
                        this.setState({keystorePending: false});
                        this.setState({error: null, success: `Call for <strong>${this.state.callParams.entrypoint}</strong> sent with ID <small>${operation.operationGroupID.replace(/['"]+/g, '')}</small>`});
                        window.scrollTo(0, 0);
                }).catch(error => {
                    console.log(error);
                    this.setState({error: 'Call could not be processed', keystorePending: false});
                });
        }).catch(error => {
            console.log(error);
            this.setState({error: 'Invalid keystore paasword', keystorePending: false});
        });     
    }
  }

  

  render() {
    return (
        <div className="main-content">
            <AccountNavbar expUrl={this.state.expUrl}  network={this.state.network} indexer={this.state.indexer}/>
            <Container className="mt-3" fluid style={{minHeight: "calc(100vh - 100px)"}}>
            {this.state.success &&
                   <Row>
                    <Col xs="12" md="6">
                        <div className="bg-yellow text-dark p-2 rounded text-sm d-block mb-2">
                            <span dangerouslySetInnerHTML={{__html: this.state.success}}>
                            </span>
                            <small className="d-block">It might take take up to 1 min. to be validated and appear on the chain.</small>
                        </div>
                    </Col>
                   </Row>
            }
            <Row className="card-operation mb-4">
                <Col md="3" className="d-none d-md-block"></Col>
                <Col xs="12" md="6">
                    <Card className="card-stats card-blur mt-5">
                        <CardBody>
                        <Row>
                                <div className="col">
                                    <CardTitle
                                        tag="h2"
                                        className="text-uppercase text-secondary mb-2"
                                    >
                                        Set custom contract
                                    </CardTitle>
                                </div>
                                <Col className="col-auto">
                                    <div className="icon icon-shape bg-primary text-dark rounded-circle shadow">
                                        <i className="fas fa-exchange-alt"></i>
                                    </div>
                                </Col>
                        </Row>
                        <div>
                            {!this.state.entryPoints && <div>
                                    <small className="text-white mt-3 mb-2 mr-2">Select network </small>
                                    <Dropdown isOpen={this.state.dropdownNetwork} toggle={this.toggleNetwork} size="sm">
                                    <DropdownToggle caret color="success">
                                        {this.state.network.charAt(0).toUpperCase() + this.state.network.slice(1)}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                            {this.state.networks && this.state.networks.map(network =>
                                                <DropdownItem key={network} onClick={() => {this.switchNet(network)}}>
                                                    <span>{network.charAt(0).toUpperCase() + network.slice(1)}</span>
                                                </DropdownItem>
                                            )}
                                    </DropdownMenu>
                                    </Dropdown>
                            </div>}
                            <div className="mt-3">
                                <small className="text-white mt-3 mb-2 mr-2">Select wallet </small>
                                {this.state.userData && <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown} size="sm">
                                  <DropdownToggle caret color="primary">
                                     {this.state.userData.alias && this.state.userData.alias[this.state.indexer]?
                                        `${this.state.userData.alias[this.state.indexer]} (${truncStringPortion(this.state.userData.value[this.state.indexer], 6, 3)})`
                                     :
                                        this.state.userData.value[this.state.indexer]
                                     }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                        {this.state.userData && this.state.userData.value.map((key, index) =>
                                            <DropdownItem key={key} onClick={() => {this.switchWallet(index)}}>
                                                {this.state.userData.type[index] === 'ledger'?
                                                    <i className="fab fa-usb text-primary align-middle mr-2"></i>
                                                :
                                                    this.state.userData.type[index] === 'file'?
                                                        <i className="fas fa-file text-primary align-middle mr-2"></i>
                                                    :
                                                        <i className="fas fa-wallet text-primary align-middle mr-2"></i>
                                                }
                                                <span>
                                                    {this.state.userData.alias && this.state.userData.alias[index]?
                                                        `${this.state.userData.alias[index]} (${truncStringPortion(key, 6, 3)})`
                                                    :
                                                        key
                                                    }
                                                </span>

                                            </DropdownItem>
                                        )}
                                  </DropdownMenu>
                                </Dropdown>}
                                {this.state.copied?
                                    <small className="ml-2 text-success">copied!</small>
                                :
                                    <i className="fas fa-copy cursor-pointer ml-2 text-primary"
                                        onClick={() => {this.processCopy()}}></i>
                                }
                            </div>
                            {this.state.error && <p className="text-danger">{this.state.error}</p>}
                            <Col md="12">
                                {!this.state.entryPoints?
                                    <div>
                                            <p className="mt-3 mb-1 text-secondary text-sm">Contract address</p>
                                            <Input type="text" placeholder="KT1... address" className="ml-1 border-0" 
                                                        onChange={(e) => this.updateContractAddr(e)}/>
                                    </div>
                                :
                                    <div>
                                        <p className="mt-3 mb-1 text-secondary text-sm">
                                            <strong>{this.state.contractAddr}</strong> loaded
                                        </p>
                                    </div>
                                }
                                {this.state.entryPoints && 
                                <div>
                                    <p className="mt-3 mb-1 text-secondary text-sm">Select entrypoint</p>
                                    <Dropdown isOpen={this.state.dropdownEntrypointsOpen} toggle={this.toggleDropdownEntrypoints} size="sm">
                                    <DropdownToggle caret color="primary">
                                        {this.state.selectedEntrypoint?
                                            this.state.selectedEntrypoint.name
                                        :
                                            'Please select Entrypoint'
                                        }
                                    </DropdownToggle>
                                    <DropdownMenu>
                                            {this.state.entryPoints.map((entry, index) => !entry.name.includes('get') &&
                                                <DropdownItem key={entry.name} onClick={() => {this.setState({selectedEntrypoint: entry, entries: []})}}>
                                                    <span>
                                                        {entry.name}
                                                    </span>
                                                </DropdownItem>
                                            )}
                                    </DropdownMenu>
                                    </Dropdown>
                                    {this.state.selectedEntrypoint && this.state.selectedEntrypoint.parameters.map(pp => (
                                            pp.optional?
                                                <Col xl={8} sm={12} key={pp.type} className="mb-3" key={pp.name}>
                                                    <p className="mt-3 mb-1 text-secondary text-sm">{pp.type} of type {pp.constituentType}</p>
                                                    <Input type="text" placeholder={pp.constituentType} ref={this['ref_'+pp.type]} className="ml-1 border-0"
                                                     onChange={(e) => this.updateEntry(e, pp.name)}/>
                                                </Col>
                                            :
                                                <Col xl={8} sm={12} key={pp.name} className="mb-3" key={pp.name}>
                                                    <p className="mt-3 mb-1 text-secondary text-sm">{pp.name} of type {pp.type}</p>
                                                    <Input type="text" placeholder={pp.type} ref={this['ref_'+pp.name]} className="ml-1 border-0"
                                                    onChange={(e) => this.updateEntry(e, pp.name)}/>
                                                </Col>
                                    ))}
                                </div>}
                            </Col>
                            {this.state.selectedEntrypoint && this.state.userData && this.state.userData.type[this.state.indexer] === 'file' && JSON.stringify(this.state.userData.keystore[this.state.indexer]) === '{}' &&
                                <Col md="12" className="mt-4">
                                    <input
                                            placeholder="Keystore file"
                                            type="file" className="d-none"
                                            ref={this.hiddenFileInput}
                                            onChange={(e) => this.onFileHandler(e)}
                                    />
                                    {!this.state.encryptedWallet?
                                            <Button color="primary" type="button" onClick={this.handleUpload}>
                                                Upload Keystore file
                                            </Button>
                                    :
                                            <div>
                                            <p className="text-yellow mb-1">
                                                <small><strong>{this.state.fileName}</strong> selected</small>
                                            </p>
                                            <p className="text-info cursor-pointer mt-1" onClick={() => {this.setState({encryptedWallet: null})}}>
                                                <small>Click to change file</small>
                                            </p>
                                            </div>
                                    }
                                </Col>
                            }
                            {this.state.selectedEntrypoint && this.state.userData && this.state.userData.type[this.state.indexer] === 'file' &&
                                <Col md="12">
                                    <p className="mt-3 mb-1 text-secondary text-sm">Password</p>
                                    <Input type="password" placeholder="Password to unlock keystore file" className="ml-1 border-0"
                                    onChange={(e) => this.updatePassword(e)}/>
                                </Col>
                            }
                            {this.state.feesTez > 0 && <p className="mt-3 mb-1 text-secondary text-sm">
                                Estimated fees:
                                <strong className="ml-2 text-yellow mr-2">{parseFloat(this.state.feesTez/1000000)} ꜩ</strong>
                                | Gas required:
                                <strong className="ml-2 text-yellow mr-2">{parseFloat(this.state.burnTez)}</strong>
                            </p>}
                        </div>
                        </CardBody>
                        <CardFooter className="bg-transparent text-white">
                            {!this.state.entryPoints && <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={this.fetchContract}>Set contract</Button>}
                            {this.state.selectedEntrypoint && !this.state.keystorePending && !this.state.ledgerPending && !this.state.callParams && <Button size="sm" color="primary" className="bg-primary border-0 p-2" onClick={this.buildOp}>Simulate operation</Button>}
                            {!this.state.ledgerPending && !this.state.keystorePending && this.state.callParams && <Button size="sm" color="success" className="bg-gradient-success border-0 p-2" onClick={this.sendOperation}>Send operation</Button>}
                            {this.state.ledgerPending &&
                                <small className="text-secondary">Please connect your Ledger and follow instructions...</small>
                            }
                            {this.state.keystorePending &&
                                <small className="text-secondary">Please wait while processing...</small>
                            }
                            <Button size="sm" color="secondary" className="bg-gradient-secondary border-0 p-2 float-right" onClick={() => {window.location.href=`/account/dashboard?network=${this.state.network}&indexer=${this.state.indexer}`}}>Cancel</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col md="3" className="d-none d-md-block"></Col>
            </Row>
            <br/>
            </Container>
            <AccountFooter />
        </div>
    );
  }
}

export default Contract;
