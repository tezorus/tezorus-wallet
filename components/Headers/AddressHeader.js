import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Container,
  Row,
} from "reactstrap";
import {shrinkString} from '../../utils/helper.js';

class AddressHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        copied: false,
        dropdownOpen: false
    };
  };

  processCopy = () =>{
    this.props.copyElement(this.props.address);
    this.setState({copied: true});
    setTimeout(
        function() {
            this.setState({copied: false});
        }
        .bind(this),
        2000
    );
  }
  render() {
    return (
      <>
        <div className="header pb-6 pb-md-8 pt-2 pt-md-5">
          <Container fluid>
            <div className="header-body">
              {/* Card stats */}
              <Row>
                <Col xs="12" xl="6">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Selected wallet
                          </CardTitle>
                            <h4 className="text-yellow d-inline">{this.props.address}</h4>
                            {this.state.copied?
                                <small className="ml-2 text-primary">copied!</small>
                            :
                                <i className="fas fa-copy cursor-pointer ml-2 text-primary"
                                    onClick={() => {this.processCopy()}}></i>
                            }
                        </div>
                        <Col className="col-auto d-none d-md-block">
                          <div className="icon icon-shape bg-gradient-primary text-white rounded-circle shadow">
                            <i className="fas fa-wallet" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-secondary text-sm">
                        <span className="text-nowrap">INFO <i className="fas fa-chevron-right"></i>
                            <strong className="ml-2 mr-2">
                                Public address | Read-only
                            </strong>
                        </span>
                      </p>
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            XTZ Balance
                          </CardTitle>
                          {this.props.accountDetails?
                              <span className="h2 font-weight-bold mb-0 text-yellow shadow-sm">
                                {this.props.accountDetails.spendable_balance}
                              </span>
                          :
                              <span className="h2 font-weight-bold mb-0 text-info ml-4">
                                <i className="fas fa-circle-notch fa-spin"></i>
                              </span>
                          }
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-gradient-primary text-white rounded-circle shadow">
                            <strong>ꜩ</strong>
                          </div>
                        </Col>
                      </Row>
                      {this.props.accountDetails && <p className="mt-3 mb-0 text-secondary text-sm">
                        {!this.props.accountDetails.is_revealed?
                            <strong className="text-white mr-2">
                              <i className="fa fa-eye-slash" /> Not revealed |
                            </strong>
                        :
                            <strong className="text-white mr-2">
                              <i className="fa fa-eye" /> Revealed |
                            </strong>
                        }
                        {this.props.accountDetails.is_revealed &&
                            <span className="text-nowrap">Ready to operate</span>
                        }
                      </p>}
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        <small className="text-secondary p-2 d-block text-md">Read-only wallet</small>
                    </CardFooter>
                  </Card>
                </Col>
                <Col xs="12" lg="6" xl="3">
                  <Card className="card-stats mb-4 mb-xl-0 card-blur">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-secondary mb-0"
                          >
                            Network
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0 text-white">
                            {this.props.network &&
                                this.props.network.charAt(0).toUpperCase() + this.props.network.slice(1)}
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                            <i className="fas fa-project-diagram" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-secondary text-sm">
                        Block Id <strong className="text-yellow">#{this.props.blockHead.toLocaleString()}</strong> | Cycle <strong className="text-yellow">{this.props.cycle}</strong>
                      </p>
                    </CardBody>
                    <CardFooter className="bg-transparent text-white">
                        <small>
                            <span className="text-nowrap">Switch to </span>
                            <Button size="sm" color="primary" className="bg-gradient-success border-0 p-2 ml-1" onClick={() => {this.props.switchNetwork()}}>
                               {this.props.network === 'mainnet'? 'Delphinet' :  'Mainnet'}
                            </Button>
                        </small>
                    </CardFooter>
                  </Card>
                </Col>
                {this.props.tokens && this.props.tokens.map((token) => (
                    <Col xs="12" lg="6" xl="3" key={token.token_symbol}>
                      <Card className="card-stats mb-4 mb-xl-0 mt-4 card-blur">
                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle
                                tag="h5"
                                className="text-uppercase text-secondary mb-0"
                              >
                                {token.token_name}
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-yellow">
                                {token.value.balance}
                              </span>
                            </div>
                            <Col className="col-auto">
                              <div className="icon icon-shape bg-gradient-primary text-white rounded-circle shadow">
                                {token.token_symbol}
                              </div>
                            </Col>
                          </Row>
                          <p className="mt-3 mb-0 text-secondary text-sm">
                            <span className="text-nowrap"><small>{token.about && shrinkString(token.about.description, 38, 3)}</small></span>
                          </p>
                        </CardBody>
                      </Card>
                    </Col>
                ))}
              </Row>
            </div>
          </Container>
        </div>
      </>
    );
  }
}

export default AddressHeader;
